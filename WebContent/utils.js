

function getMainPage(urlstring,divname){
	 //alert('urlstring'+urlstring);
	$.ajax({
		  type: 'GET',
		  url: urlstring+(urlstring.indexOf('?')>0?'&':'?')+'t='+Math.random(),
		  beforeSend: function(xhr){     
			 
		    //$('.loading-image').show();
		  },
		  success: function(msg){
			  //alert($('#content').html());
			  //alert('success!!:'+msg);
			  //alert($('#content').html());
			// alert(msg);
			$('.loading-image').hide();
			if(divname==null){
				$('#content').html('');
				$('#content').html(msg);
			}else{
				$('#'+divname).html('');
				$('#'+divname).html(msg);
			}

		  }, 
          error: function (xhr, status) {
	            alert('Unknown error ' + status); 
	          } 
	});
	
}


function postForm(servletname,formname,confmsg){
	var proceed=true;
	//alert(typeof confmsg);
	if(confmsg!='' && confmsg!=null && confmsg!=undefined){ 
		proceed=confirm(confmsg);
	}
	//alert('proceed:'+proceed);
	if(proceed){
		try{
			//alert('serialize:'+$(formname).serialize());
			$.post(servletname, $('#'+formname).serialize(), function(data){
				//alert(data);
		        //alert('Form Submitted!!'); 
		        $('#content').html(data);
		    });
			
			//Serialize looks good name=textInNameInput&&telefon=textInPhoneInput---etc
		    /*.done(function(data) {
		        alert(data);
		    }*/
		    
		}catch(e){
			alert(e);
		}
	}
}
/*
function provideSolution(servletname,formname,confmsg){
	
	 postForm(servletname,formname,confmsg);
	
}
*/
function searchSolution(searchstr,modelid){
	try{
		//alert('serialize:'+$(formname).serialize());
		searchstr=escape($('#'+searchstr).val());
		modelid=$('#'+modelid).val();
		var servletname='SearchSolution?searchstr='+searchstr+'&modelid='+modelid+'&t='+Math.random();
		
		$('#suggestion').html("<img  id='LoadingImage' width='50px' height='50px' src='img/loading-blue.gif' >");
		
		$.ajax({ 
	          type: "GET", 
	          url: servletname, 
	          dataType: "text", 
	          cache : false, 
	          success: function(response) { 
	  	        $('#suggestion').html(response);
	          }, 
	          error: function (xhr, status) {  
	            $("#LoadingImage").hide();
	            alert('Unknown error ' + status); 
	          }    
	       });  
		
	}catch(e){
		alert(e);
	}
}


function calldropdown(currentobj,tochange,sqlname){
	try{
		//alert('serialize:'+$(formname).serialize());
		//searchstr=escape($('#'+searchstr).val());
		var sql=$('#'+sqlname).val();
		var currval=$('#'+currentobj).val();
		var servletname='callDropDown?sql='+sql.replace(/\?/g,currval)+'&t='+Math.random();
		
		 
		$.ajax({ 
	          type: "GET", 
	          url: servletname, 
	          dataType: "text", 
	          cache : false, 
	          success: function(response) { 
	        	$("#"+tochange).html('');
	        	$("#"+tochange).append("<option value=''>Select</option>");
	        	$("#"+tochange).html(response);
	        	//alert(response);
	        	/*var list=response.split('|');
	        	for(var i=0;i<values.length;i++){
	        		$("#"+tochange).append("<option value='"+list[i].split(':')[0]+"'>"+list[i].split(':')[1]+"</option>");
	        	}*/
	          }, 
	          error: function (xhr, status) {
	            alert('Unknown error ' + status); 
	          }    
	       });  
		
	}catch(e){
		alert(e);
	}
}


function validatestock(i){
	//alert('1');
	if($('#select'+i).is(':checked')){
		//alert('2');
		if($("#quantityreplace"+i).val()=='' || parseInt($("#currentstock"+i).val())==0 || parseInt($("#currentstock"+i).val())<parseInt($("#quantityreplace"+i).val())){
			//alert('3');
			alert('Stock not available!!!');
			$("#quantityreplace"+i).val('');
			//$("#select"+i).prop( {checked: false} );
			document.getElementById("select"+i).checked=false;
		}
		
	}
}



function openpopup(urlstring,urlvalue){
	if(urlvalue!=''){
		urlvalue=urlvalue.split(',');
		for(var i=0,j=1;i<urlvalue.length;i++,j++){
			urlstring=urlstring.replace('$'+j,document.getElementById(urlvalue[i]).value);
		}
	}
	//alert(urlstring);
	window.open(urlstring,"myWindow","toolbar=no, scrollbars=yes, resizable=yes, top=500, left=500, width=800, height=800");
}