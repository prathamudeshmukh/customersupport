

import htmlComponents.HTMLComponents;
import smsSender.SMSUtil;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dbComponents.ConnectDB;

/**
 * Servlet implementation class ComplaintAcceptance
 */
@WebServlet("/ComplaintAcceptance")
public class ComplaintAcceptance extends HttpServlet {
	private static final long serialVersionUID = 1L;
	//private SendSMS smsSender;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ComplaintAcceptance() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		Connection connection= ConnectDB.newConnection("customersupport");
		PreparedStatement preparedstatement;
		
		HttpSession session=request.getSession();
		Statement statement;
		ResultSet resultset;
		String sqlString="";
		String errMsg="",succMsg="",validMsg="";
		
		//String username =(String) session.getAttribute("username");
		int userregisterid =(Integer) session.getAttribute("userregisterid");
		int usertypeid =(Integer) session.getAttribute("usertypeid");
		String button =ConnectDB.getParam(request, "button");
		String solutiondetails=ConnectDB.getParam(request, "solutiondetails");
		int accepttransid=Integer.parseInt(ConnectDB.getParam(request, "accepttransid"));
		String complaintregisterid=ConnectDB.getParam(request, "complaintregisterid");
		String solutiontransid=ConnectDB.getParam(request, "solutiontransid");
		
		
		
		
		if(button.equals("Send") ){
			if(solutiondetails.equals("")){
				validMsg="Enter Solution";
			}else{
				try {
					
					sqlString="INSERT INTO solutiontrans( "+
					" solutiontransid, accepttransid, solution, solutionby, solutiondate) "+
	            	" VALUES ((select coalesce(max(solutiontransid),0)+1 from solutiontrans ), ?, ?, ?, now());";
					preparedstatement=connection.prepareStatement(sqlString);
					preparedstatement.setInt(1, accepttransid);
					preparedstatement.setString(2, solutiondetails);
					preparedstatement.setInt(3, userregisterid);
					preparedstatement.executeUpdate();
					
					int totrows=(!ConnectDB.getParam(request, "totrows").equals(""))?Integer.parseInt(ConnectDB.getParam(request, "totrows")):0;
					for(int i=1;i<=totrows;i++){
						String select=ConnectDB.getParam(request, "select"+i);
						if(select.equals("on")){
							int quantityreplace=Integer.parseInt(ConnectDB.getParam(request, "quantityreplace"+i));
							int partsid=Integer.parseInt(ConnectDB.getParam(request, "partsid"+i));
							sqlString="INSERT INTO stocktrans( partid, stockquantity, accepttransid,complaintregisterid)"+
						    "VALUES ( ?, ?, ?,?);";
							preparedstatement=connection.prepareStatement(sqlString);
							preparedstatement.setInt(1, partsid);
							preparedstatement.setInt(2, quantityreplace);
							preparedstatement.setInt(3, accepttransid);
							preparedstatement.setInt(4, Integer.parseInt(complaintregisterid));
							preparedstatement.executeUpdate();
							
							sqlString="update partsmaster set currentstock=coalesce(currentstock,0)-? where partsid="+partsid;
							preparedstatement=connection.prepareStatement(sqlString);
							preparedstatement.setInt(1,quantityreplace);
							preparedstatement.executeUpdate();
						}
					}
					try{
						sqlString="select telephone from userregister inner join complaintregister cg on cg.complaintby=userregister.userregisterid where complaintregisterid="+complaintregisterid;
						String telephone=ConnectDB.getResult(sqlString);
						SMSUtil.SendSMS("Solution provided for your Complaint No."+complaintregisterid+" : "+solutiondetails, telephone);

						}
					catch (Exception e) {
						// TODO Auto-generated catch block
						System.out.println("SEND SMS Error"+e);
					}
					succMsg="Solution provided succesfully!!";
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					System.out.println(e);
				}
				
				
				
			}
		}
		
		if(button.equals("Provide")){
			try {
				
				sqlString="INSERT INTO solutiontrans( solutiontransid, accepttransid, solution, solutionby, solutiondate,refsolutiontransid) "+
				" select (select coalesce(max(solutiontransid),0)+1 from solutiontrans ), ?, solution, ?, now(),solutiontransid from solutiontrans "+
				" where solutiontransid=?";
				preparedstatement=connection.prepareStatement(sqlString);
				preparedstatement.setInt(1, accepttransid);
				//preparedstatement.setString(2, solutiondetails);
				preparedstatement.setInt(2, userregisterid);
				preparedstatement.setInt(3, Integer.parseInt(solutiontransid));
				preparedstatement.executeUpdate();
				
				try{
					sqlString="select telephone from userregister inner join complaintregister cg on cg.complaintby=userregister.userregisterid where complaintregisterid="+complaintregisterid;
					String telephone=ConnectDB.getResult(sqlString);
					SMSUtil.SendSMS("Solution provided for your Complaint No."+complaintregisterid, telephone);

					}
				catch (Exception e) {
					// TODO Auto-generated catch block
					System.out.println("SEND SMS Error"+e);
				}
				
				succMsg="Solution provided succesfully!!";
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				out.println(e);
			}
		}
		
		if(button.equals("Forward")){
			try {
				
						
				statement=connection.createStatement();
				sqlString="select * from ( "+
						" select ur.userregisterid,ur.usertypeid,count(at.*) as totpendcount from userregister ur "+
						" left join complaintaccepttrans at on at.acceptedby=ur.userregisterid and at.solvedflag=0 and forwardflag=0 "+
						" where ur.usertypeid="+(usertypeid+1)+" "+
						" group by ur.userregisterid,ur.usertypeid "+
						" ) x "+
						" order by totpendcount "+
						" limit 1";
						
						resultset=statement.executeQuery(sqlString);
						if(resultset.next()){
							sqlString="update complaintaccepttrans set forwardflag=1,forwarddate=now(),forwardto=? where accepttransid="+accepttransid;
							preparedstatement=connection.prepareStatement(sqlString);
							preparedstatement.setInt(1, resultset.getInt("userregisterid"));
							preparedstatement.executeUpdate();
							
							sqlString="INSERT INTO complaintaccepttrans( "+
									" accepttransid, complaintregisterid, acceptedby, accepteddate, usertypeid) "+
									" VALUES ((select coalesce(max(accepttransid),0)+1 from complaintaccepttrans ), ?, ?, now(),?); ";
							preparedstatement=connection.prepareStatement(sqlString);
							preparedstatement.setInt(1, Integer.parseInt(complaintregisterid));
							preparedstatement.setInt(2, resultset.getInt("userregisterid"));
							preparedstatement.setInt(3, resultset.getInt("usertypeid"));
							preparedstatement.executeUpdate();
							
							try{
								sqlString="select coalesce(telephone,'') from userregister where userregisterid="+resultset.getInt("userregisterid");
								String telephone=ConnectDB.getResult(sqlString);
								sqlString="select complaintdetails from complaintregister where complaintregisterid="+complaintregisterid;
								String complaintdetails=ConnectDB.getResult(sqlString);
								
								SMSUtil.SendSMS("A complaint is forwarded to you, No."+complaintregisterid+" : "+complaintdetails, telephone);

								}
							catch (Exception e) {
								// TODO Auto-generated catch block
								System.out.println("SEND SMS Error"+e);
							}
							
							succMsg="Complaint Forwarded succesfully!!";
						}
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				out.println(e);
			}
		}
		
		
		
		
		
		
		out.println("<div id='box'>");
		if(!errMsg.equals("")){
			out.println("<div class='error'>"+errMsg+"</div>");
		}
		if(!succMsg.equals("")){
			out.println("<div class='success'>"+succMsg+"</div>");
			out.println("<script> $('#content').html('');</script>");
		}
		if(!validMsg.equals("")){
			out.println("<div class='validation'>"+validMsg+"</div>");
		}
		out.println("<h3>Complaint Acceptance</h3>");
		out.println("<form id='form' name='form' action='' method='post'>");
		
		sqlString="select complainttype.complaintdescription, complaintregister.complaintdetails,p.productname||' - '||m.modelname as modelname,case when complaintregister.complaintdate<=ms.warrantyexpirydate then '<b>(In warranty)</b>' else '<b>(Out of warranty)</b>' end as status from complaintregister "+
		" inner join complainttype on complainttype.complainttypeid=complaintregister.complainttypeid "+
		" inner join modelsmaster m on m.modelid=complaintregister.modelid "+
		" inner join productmaster p on p.productid=m.productid "+
		" inner join machinesale ms on ms.registrationid=complaintregister.complaintby "+
		" where complaintregisterid="+complaintregisterid;
		try{
			statement=connection.createStatement();
			resultset=statement.executeQuery(sqlString);
			if(resultset.next()){
				out.println("<fieldset id='c'>");
				out.println("<label for='product'>Product/Model:</label>"+resultset.getString("modelname")+" "+resultset.getString("status"));
				out.println("</fieldset >");
				
				out.println("<fieldset id='a'>");
				out.println("<label for='complainttypeid'>Complaint Type:</label>"+resultset.getString("complaintdescription"));
				out.println("</fieldset >");
				
				out.println("<fieldset id='b'>");
				out.println("<label for='complaintdetails'>Complaint Details:</label>"+resultset.getString("complaintdetails"));
				out.println("</fieldset >");
			}
		}catch (SQLException e)
		{
			out.println(e);
		}
		
		out.println("<fieldset id='c'>");
		out.print("<label for='solutiondetails'>Solution:</label>");
		out.println("<textarea name='solutiondetails' id='solutiondetails' rows='3' cols='50'></textarea>");
		out.println("</fieldset>");
		
		if(usertypeid==5){
			out.println("<h3>Part Replacement</h3>");
			out.println("<div style='height:250px;overflow-y: auto;'>");
			out.println("<table width='100%'>");
			out.println("<thead>");
			out.println("<tr>");
			out.println("<th >Parts</th>");
			out.println("<th >Stock Qty.</th>");
			out.println("<th >Price</th>");
			out.println("<th >Qty. Replaced</th>");
			out.println("<th >select</th>");
			out.println("</tr>");
			out.println("</thead>");
			out.println("<tbody>");
			sqlString=" select partsid,partname,currentstock,price from partsmaster where currentstock>0 "+
			" order by partname ";
			int i=0;
			try{
				statement=connection.createStatement();
				resultset=statement.executeQuery(sqlString);
				
				while(resultset.next()){
					i++;
					out.println("<tr>");
					out.println("<td >"+resultset.getString("partname")+"</td>");
					out.println("<td  align='center'>"+resultset.getString("currentstock")+"</td>");
					out.println("<td  align='center'>"+resultset.getString("price")+"</td>");
					out.println("<td align='center'><input type='text' size='5' name='quantityreplace"+i+"' onblur='validatestock("+i+")' id='quantityreplace"+i+"' ></td>");
					out.println("<td align='center'><input type='checkbox' name='select"+i+"' id='select"+i+"' onchange='validatestock("+i+")' value='on'></td>");
					out.println("<input type='hidden' name='partsid"+i+"' id='partsid"+i+"' value='"+resultset.getString("partsid")+"'> ");
					out.println("<input type='hidden' name='currentstock"+i+"' id='currentstock"+i+"' value='"+resultset.getString("currentstock")+"'> ");
					out.println("</tr>");
					//out.println("<td ><input type='button' value='Add New'></td>");
				}
			}catch (SQLException e)
			{
				out.println(e);
			}
			out.println("</tbody>");
			out.println("</table>");
			out.println("</div>");
			out.println("<input type='hidden' name='totrows' id='totrows' value='"+i+"'> ");
		}
		out.println("<div align='center'>");
		out.println("<input id='button1' name='button' type='button' value='Submit'  onclick='postForm(\"ComplaintAcceptance?button=Send\",\"form\",\"Are you sure to provide this solution?\")' > ");
		if(usertypeid<=4){
			out.println("<input id='button1' name='button' type='button' value='Forward' onclick='postForm(\"ComplaintAcceptance?button=Forward\",\"form\",\"Are you sure to forward this complaint?\")' /> ");
		}
		out.println("<input id='button2' type='reset' />");
		out.println("</div>");
		
		
		out.println("<input type='hidden' name='accepttransid' id='accepttransid' value='"+accepttransid+"'> ");
		out.println("<input type='hidden' name='complaintregisterid' id='complaintregisterid' value='"+complaintregisterid+"'> ");
		out.println("<input type='hidden' name='solutiontransid' id='solutiontransid' value='"+solutiontransid+"'> ");
		
		out.println("</form>");
		
		sqlString="select * from ( "+
		" select distinct on (cr.complaintregisterid) cr.complaintregisterid,st.averagemarks,st.solutiontransid,m.modelname,complaintdetails,st.solution||'<br>Provided By <b>'||ur.firstname||'</b> on <b>'||to_char(st.solutiondate,'dd-mm-yyyy HH:MI')||'</b>' as solution,count(tag.*) as cnt,array_agg(originalword) as tags from complaintregister cr "+
		" inner join tagwordsallocation tag on tag.complaintregisterid=cr.complaintregisterid "+
		" inner join complaintaccepttrans ct on ct.complaintregisterid=cr.complaintregisterid and ct.solvedflag=1 "+
		" inner join solutiontrans st on st.accepttransid=ct.accepttransid and st.solvedflag=1 "+
		" inner join userregister ur on ur.userregisterid=st.solutionby "+
		" inner join modelsmaster m on m.modelid=cr.modelid "+
		" where cr.solvedflag =1 and tag.wordid in ( "+
		" select wordid from tagwordsallocation where complaintregisterid="+complaintregisterid+" "+
		" ) and m.productid in (select productid from complaintregister inner join modelsmaster on modelsmaster.modelid=complaintregister.modelid where complaintregisterid="+complaintregisterid+") "+
		" and st.refsolutiontransid is null and st.solutiontransid not in ( "+
		" 	select refsolutiontransid  from solutiontrans "+
		"	inner join complaintaccepttrans ct on ct.accepttransid=solutiontrans.accepttransid "+
		" 	where complaintregisterid="+complaintregisterid+" "+
		" ) "+
		" group by cr.complaintregisterid,st.averagemarks,st.solutiontransid,m.modelname,complaintdetails,st.solution,ur.firstname,st.solutiondate "+
		" ) x "+
		" order by cnt desc,averagemarks desc ";
		//out.println(sqlString);
		try{
			statement=connection.createStatement();
			resultset=statement.executeQuery(sqlString);
			while(resultset.next()){
				//
				if(resultset.isFirst()){
					out.println("<h3>Suggested solutions</h3>");
					out.println("<form>");
					out.println("<table width='100%'><thead><tr>");
					out.println("<th>Model</th>");
					out.println("<th>Complaint</th>");
					out.println("<th>Solution</th>");
					out.println("<th>Tags</th>");
					out.println("<th>Average Rating</th>");
					out.println("<th>Select</th>");
					out.println("</tr></thead>");
				}
				
				out.println("<tr><td>"+resultset.getString("modelname")+"</td>");
				out.println("<td>"+resultset.getString("complaintdetails")+"</td>");
				out.println("<td>"+resultset.getString("solution")+"</td>");
				out.println("<td>"+resultset.getString("tags")+"</td>");
				out.println("<td align='center'><font size='25' weight='100'>"+resultset.getDouble("averagemarks")+"</font></td>");
				out.println("<td align='center'><a href='#' onclick='postForm(\"ComplaintAcceptance?button=Provide&solutiontransid="+resultset.getString("solutiontransid")+"\",\"form\",\"Are you sure to provide this solution?\")'>Provide</a></td>");
				out.println("</tr>");
				
				if(resultset.isLast()){
					out.println("</table>");
					out.println("</form>");
				}

			}
		}catch (SQLException e)
		{
			out.println(e);
		}

		/*out.println("<fieldset id='d'>");
		out.println("<label for='previousSolution'>Previous Solution</label>");
		out.println("</fieldset >");*/
		
		
		
		sqlString="select to_char(sat.solutiondate,'dd-mm-yyyy HH:MI') as solutiondate,sat.solution||'<br>Provided By <b>'||ur.firstname||'</b> on <b>'||to_char(sat.solutiondate,'dd-mm-yyyy HH:MI')||'</b>' as solution,sat.feedback||'<br>On <b>'||to_char(sat.feedbackdate,'dd-mm-yyyy HH:MI')||'</b>' as feedback from complaintregister "+ 
				" inner join complaintaccepttrans cat on cat.complaintregisterid=complaintregister.complaintregisterid and cat.solvedflag=0 "+
				" inner join solutiontrans sat on sat.accepttransid=cat.accepttransid and feedbackflag=1 "+ 
				" inner join userregister ur on ur.userregisterid=sat.solutionby "+
				" where complaintregister.complaintregisterid="+complaintregisterid+
				" order by sat.feedbackdate desc ";
		try{
			statement=connection.createStatement();
			resultset=statement.executeQuery(sqlString);
			while(resultset.next()){
				if(resultset.isFirst()){
				out.println("<h3>Previous Solution</h3>");
				out.println("<form>");
				out.println("<table width='100%'><thead><tr>");
				out.println("<th>Solution</th>");
				out.println("<th>Feedback</th>");
				out.println("</tr></thead>");
				}
				//out.println("<td>"+resultset.getString("solutiondate")+"</td>");
				out.println("<tr><td>"+resultset.getString("solution")+"</td>");
				out.println("<td>"+resultset.getString("feedback")+"</td></tr>");
				//out.println("<td>"+resultset.getString("feedbackdate")+"</td>");
				if(resultset.isLast()){
					out.println("</table>");
					out.println("</form>");
					
				}
			}
		}catch (SQLException e)
		{
			out.println(e);
		}
		out.println("</div>");

	}

	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request,response);
	}
	

}
