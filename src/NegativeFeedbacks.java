

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dbComponents.ConnectDB;

/**
 * Servlet implementation class NegativeFeedbacks
 */
@WebServlet("/NegativeFeedbacks")
public class NegativeFeedbacks extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public NegativeFeedbacks() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		Connection connection= ConnectDB.newConnection("customersupport");

		HttpSession session=request.getSession();
		Statement statement;
		ResultSet resultset;
		String sqlString="";
		
		//String username =(String) session.getAttribute("username");
		int userregisterid =(Integer) session.getAttribute("userregisterid");
		
		out.println("<div id='box'>");
		
		
		boolean rowsreturned=false;
		
		sqlString="select * from ( select at.accepttransid,cr.complaintregisterid,complaintdescription,complaintdetails,(select count(*) AS cnt from solutiontrans where solutiontrans.accepttransid=at.accepttransid and feedbackflag=1  and feedbackratingid in (3,4)) as cnt,(select count(*) AS cnt from solutiontrans where solutiontrans.accepttransid=at.accepttransid and feedbackflag=0)  as action  from complaintregister cr "+
				" inner join complaintaccepttrans at on at.complaintregisterid=cr.complaintregisterid "+
				" inner join complainttype ct on ct.complainttypeid=cr.complainttypeid "+
				" inner join (select count(*) AS cnt,solutiontrans.accepttransid from solutiontrans group by solutiontrans.accepttransid ) sat on sat.accepttransid=at.accepttransid  "+
				" where at.acceptedby="+userregisterid+" and at.solvedflag=0 and forwardflag=0 "+
				" )x "+
				" where cnt>0";
		try{
			statement=connection.createStatement();
			resultset=statement.executeQuery(sqlString);
			while(resultset.next()){
				rowsreturned=true;
				if(resultset.isFirst()){
					out.println("<h3>Negative Feedbacks</h3>");
					
					out.println("<table width='100%'>");
					out.println("<thead>");
					out.println("<tr>");
					out.println("<th >Complaint Type</th>");
					out.println("<th width='70%'>Complaint</th>");
					out.println("<th >Action</th>");
					out.println("</tr>");
					out.println("</thead>");
					out.println("<tbody>");
				}
				
				out.println("<tr>");
				out.println("<td align='center'>"+resultset.getString("complaintdescription")+"</td>");
				out.println("<td>"+resultset.getString("complaintdetails")+"</td>");
				out.println("<td align='center'>");
				if(resultset.getInt("action")==0){
					out.println("<a  href='#' onclick='getMainPage(\"ComplaintAcceptance?accepttransid="+resultset.getString("accepttransid")+"&complaintregisterid="+resultset.getString("complaintregisterid")+"\")' >Solve</a>");
				}
				out.println("</td>");
				out.println("");
				out.println("</tr>");
				
				if(resultset.isLast()){
					out.println("</tbody>");
					out.println("</table>");
				}
				
			}
			
			if(!rowsreturned){
				out.println("<h3 align='center'>");
				out.println("No Negative Feedbacks to display!!");
				out.println("</h3>");
			}
			
			
		}catch(SQLException se){
			out.println(se);
		}
		
		
		

		out.println("</div>");

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request,response);
	}

}
