

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import smsSender.SMSUtil;

import dbComponents.ConnectDB;
import htmlComponents.HTMLComponents;

/**
 * Servlet implementation class RegisterComplaint
 */
@WebServlet("/RegisterComplaint")
public class RegisterComplaint extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public RegisterComplaint() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		Connection connection= ConnectDB.newConnection("customersupport");


		HttpSession session=request.getSession();

		Statement statement;
		PreparedStatement preparedstatement;

		ResultSet resultset;
		String sqlString="",validMsg="",succMsg="";

		int userregisterid =(Integer) session.getAttribute("userregisterid");
		int usertypeid=(Integer) session.getAttribute("usertypeid");


		String complainttypeid	=ConnectDB.getParam(request, "complainttypeid");
		String button =ConnectDB.getParam(request, "button");
		String complaintdetails=ConnectDB.getParam(request, "complaintdetails");
		String modelid=ConnectDB.getParam(request, "modelid");
		String complaintregisterid="";

		if(button.equals("Send")){
			if(!complaintdetails.equals("")){
				try {

					statement=connection.createStatement();
					resultset=statement.executeQuery("select coalesce(max(complaintregisterid),0)+1 from  complaintregister");
					if(resultset.next()){
						complaintregisterid=resultset.getString(1);
					}
					sqlString="INSERT INTO complaintregister( "+
							" complaintregisterid, complainttypeid, complaintdetails, complaintdate,complaintby,modelid) "+
							" VALUES (?, ?, ?,now(), ?,?);";
					preparedstatement=connection.prepareStatement(sqlString);
					preparedstatement.setInt(1, Integer.parseInt(complaintregisterid));
					if(complainttypeid.equals("")){
						preparedstatement.setNull(2,Types.INTEGER);
					}else{
						preparedstatement.setInt(2, Integer.parseInt(complainttypeid));
					}
					preparedstatement.setString(3, complaintdetails);
					preparedstatement.setInt(4, userregisterid);
					preparedstatement.setInt(5, Integer.parseInt(modelid));
					preparedstatement.executeUpdate();

					sqlString="INSERT INTO tagwordsallocation(complaintregisterid, wordid,originalword) "+
							" select distinct on(sentence) "+complaintregisterid+" as complaintregisterid,wordid,sentence from ( "+
							" select * from ( "+
							" select word,wordid,lower(unnest(string_to_array(replace(replace(?,'.',' '),',',' '),' '))) as sentence  from technicalwords "+
							" ) x "+
							" where checkword(sentence,word) "+
							" order by word "+ 
							" ) y  ";
					preparedstatement=connection.prepareStatement(sqlString);
					preparedstatement.setString(1, complaintdetails);
					preparedstatement.executeUpdate();

					sqlString="select * from ( "+
							" select ur.userregisterid,ur.usertypeid,count(at.*) as totpendcount from userregister ur "+
							" left join complaintaccepttrans at on at.acceptedby=ur.userregisterid and at.solvedflag=0 and forwardflag=0 "+
							" where ur.usertypeid=3 "+
							" group by ur.userregisterid,ur.usertypeid "+
							" order by random() * (row_number() over()) "+
							" ) x "+
							" order by totpendcount "+
							" limit 1";

					resultset=statement.executeQuery(sqlString);
					if(resultset.next()){
						sqlString="INSERT INTO complaintaccepttrans( "+
								" accepttransid, complaintregisterid, acceptedby, accepteddate, usertypeid) "+
								" VALUES ((select coalesce(max(accepttransid),0)+1 from complaintaccepttrans ), ?, ?, now(),?); ";
						preparedstatement=connection.prepareStatement(sqlString);
						preparedstatement.setInt(1, Integer.parseInt(complaintregisterid));
						preparedstatement.setInt(2, resultset.getInt("userregisterid"));
						preparedstatement.setInt(3, resultset.getInt("usertypeid"));
						preparedstatement.executeUpdate();
						
						try{
							sqlString="select telephone from userregister where userregisterid="+resultset.getInt("userregisterid");
							String telephone=ConnectDB.getResult(sqlString);
							
							SMSUtil.SendSMS("New complaint is arrived , No."+complaintregisterid+" : "+complaintdetails, telephone);

							}
						catch (Exception e) {
							// TODO Auto-generated catch block
							System.out.println("SEND SMS Error"+e);
						}
						
						 succMsg = "Complaint registered succesfully !";
					}


				} catch (SQLException e) {
					// TODO Auto-generated catch block
					out.println(e);
				}
			}else{
				validMsg="Enter the Complaint details";	
			}
		}


		out.println("<div id='box'>");
		if(!validMsg.equals("")){
			out.println("<div class='validation'>"+validMsg+"</div>");
		}if(!succMsg.equals("")){
			out.println("<div class='success'>"+succMsg+"</div>");
		}
		out.println("    	<h3 id='adduser'>Register Complaint</h3>");
		out.println("<form id='form' name='form' action='' method='post'>");
		out.println("<fieldset id='personal'>");
		//out.println("<legend>PERSONAL INFORMATION</legend>");
		out.println("<label for='complainttypeid'>Complaint Type:</label> ");
		sqlString="select * from complainttype";
		out.println(HTMLComponents.GenerateDropdown(sqlString,"complainttypeid", "complaintdescription", "complainttypeid", ""));
		out.println("<br />");
		out.println("</fieldset>");



		out.println("<fieldset id='personal'>");
		out.println("<label for='modelid'>Product/Model:</label> ");
		sqlString="select p.productname||'-'||mm.modelname as model,mm.modelid from machinesale "+
				" inner join modelsmaster mm on mm.modelid=machinesale.modelid "+
				" inner join productmaster p on p.productid=mm.productid "+
				" where registrationid="+userregisterid;
		out.println(HTMLComponents.GenerateDropdown(sqlString,"modelid", "model", "modelid", ""));
		out.println("<br />");
		out.println("</fieldset>");

		out.println("<fieldset id='opt'>");
		out.println("<label for='details'>Details : </label>");
		out.println("<textarea name='complaintdetails' id='complaintdetails' rows='3' cols='30' onkeyup='searchSolution(\"complaintdetails\",\"modelid\")'></textarea>");
		out.println("<br />");
		out.println("</fieldset>");

		out.println("<div align='center'>");
		out.println("<input id='button1' name='button' type='button' value='Send' onclick='postForm(\"RegisterComplaint?button=Send\",\"form\")'/> ");
		//out.println("<input id='button1' name='button' type='button' value='Send' onclick='x()' /> ");
		out.println("<input id='button2' type='reset' />");
		out.println("</div>");
		out.println("</form>");
		out.println("<div id='suggestion' style='overflow-y:scroll;height:150px;' valign='center' align='center'></div>");

		out.println("</div>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request,response);
	}

}
