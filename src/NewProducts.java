

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dbComponents.ConnectDB;

/**
 * Servlet implementation class NewProducts
 */
@WebServlet("/NewProducts")
public class NewProducts extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public NewProducts() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		Connection connection= ConnectDB.newConnection("customersupport");

		HttpSession session=request.getSession();
		Statement statement;
		ResultSet resultset;
		String sqlString="";
		
		//String username =(String) session.getAttribute("username");
		int userregisterid =(Integer) session.getAttribute("userregisterid");
		
		out.println("<div id='box'>");
		
		
		boolean rowsreturned=false;
		out.println("<h3>New Products</h3>");
		
		out.println("<table width='100%'>");
		out.println("<thead>");
		out.println("<tr>");
		out.println("<th >Model Name/No</th>");
		out.println("<th >Product Name</th>");
		out.println("<th >Technical Specification</th>");
		out.println("<th >Release Date</th>");
		out.println("</tr>");
		out.println("</thead>");
		out.println("<tbody>");

		sqlString=" select modelname,modelno,productname,techspecifications,to_char(productreleasedate,'dd-mm-yyyy') as productreleasedate from modelsmaster mm "+
		" inner join productmaster pm on pm.productid=mm.productid "+
		" where productreleasedate+'1 month'::interval >= now()::date and "+ 
		" pm.productid in (select ms.productid from machinesale ms where registrationid="+userregisterid+")  ";
		try{
			statement=connection.createStatement();
			resultset=statement.executeQuery(sqlString);
			while(resultset.next()){

				out.println("<tr>");
				out.println("<td>"+resultset.getString("modelname")+" "+resultset.getString("modelno")+"</td>");
				out.println("<td align='center'>"+resultset.getString("productname")+"</td>");
				out.println("<td align='center'>"+resultset.getString("techspecifications")+"</td>");
				out.println("<td align='center'>"+resultset.getString("productreleasedate")+"</td>");
				out.println("</tr>");
				
			}

			out.println("</tbody>");
			out.println("</table>");

			
			
		}catch(SQLException se){
			out.println(se);
		}
		
		
		

		out.println("</div>");

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request,response);
	}

}
