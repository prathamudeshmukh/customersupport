

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dbComponents.ConnectDB;

/**
 * Servlet implementation class StockUpdation
 */
@WebServlet("/StockUpdation")
public class StockUpdation extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public StockUpdation() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		Connection connection= ConnectDB.newConnection("customersupport");

		HttpSession session=request.getSession();
		Statement statement;
		PreparedStatement preparedstatement;
		ResultSet resultset;
		String sqlString="",succMsg="",errMsg="";
		String button	=ConnectDB.getParam(request, "button");
		String partname	=ConnectDB.getParam(request, "partname");
		String currentstock=ConnectDB.getParam(request, "currentstock");
		//String username =(String) session.getAttribute("username");
		int userregisterid =(Integer) session.getAttribute("userregisterid");

		if(button.equals("Update")){
			try {

				int totrows=Integer.parseInt(ConnectDB.getParam(request, "totrows"));
				for(int i=1;i<=totrows;i++){
					String newstock=ConnectDB.getParam(request, "newstock"+i);
					if(!newstock.equals("") ){
						String partid=ConnectDB.getParam(request, "partid"+i);
						String price=ConnectDB.getParam(request, "price"+i);
						if(Integer.parseInt(newstock)>0){ 
							sqlString=" INSERT INTO stocktrans(partid, stockquantity) "+
									" VALUES ( ?, ?); ";
							preparedstatement=connection.prepareStatement(sqlString);
							preparedstatement.setInt(1, Integer.parseInt(partid));
							preparedstatement.setInt(2, Integer.parseInt(newstock));	
							preparedstatement.executeUpdate();
						}
						sqlString="update partsmaster set currentstock=coalesce(currentstock,0)+"+newstock+",price="+price+" where partsid="+partid;
						preparedstatement=connection.prepareStatement(sqlString);
						preparedstatement.executeUpdate();

						succMsg="Stock updated successfully!!!";
					}
				}
			}catch(SQLException se){
				errMsg="error:"+se;
			}
		}

		if(button.equals("Add Part")){
			try {
				
				String price=ConnectDB.getParam(request, "price");
				
				int partsid=0; 

				sqlString=" INSERT INTO partsmaster(partsid,partname, currentstock,price) VALUES ( DEFAULT,?, ?,?); ";
				preparedstatement=connection.prepareStatement(sqlString,Statement.RETURN_GENERATED_KEYS);
				preparedstatement.setString(1,partname);
				preparedstatement.setInt(2, Integer.parseInt(currentstock));	
				preparedstatement.setDouble(3, Double.parseDouble(price));
				preparedstatement.executeUpdate();
				resultset=preparedstatement.getGeneratedKeys();
				if(resultset.next()) partsid=resultset.getInt(1);
				
				sqlString=" INSERT INTO stocktrans(partid, stockquantity) VALUES ( ?, ?); ";
				preparedstatement=connection.prepareStatement(sqlString);
				preparedstatement.setInt(1,partsid);
				preparedstatement.setInt(2, Integer.parseInt(currentstock));	
				preparedstatement.executeUpdate();

				succMsg="Part added successfully!!!";

			}catch(SQLException se){
				errMsg="error:"+se;
			}
		}


		out.println("<div id='box'>");
		if(!succMsg.equals("")){
			out.println("<div class='success'>"+succMsg+"</div>");
		}
		if(!errMsg.equals("")){
			out.println("<div class='error'>"+errMsg+"</div>");
		}
		out.println("<h3>Stock Updation</h3>");
		out.println("<form name='form' id='form'>");

		int i=0;
		if(!button.equals("Add New")){

			out.println("<table width='100%'>");
			out.println("<thead>");
			out.println("<tr>");
			out.println("<th >Part Name</th>");
			out.println("<th >Current Stock</th>");
			out.println("<th >Price</th>");
			out.println("<th >New Stock</th>");
			out.println("</thead>");
			out.println("<tbody>");
			sqlString="select partname,partsid,currentstock,price from partsmaster order by partname ";
			try{
				statement=connection.createStatement();
				resultset=statement.executeQuery(sqlString);

				while(resultset.next()){

					i++;
					out.println("<tr>");
					out.println("<td align='left'>"+resultset.getString("partname")+"</td>");
					out.println("<td align='center'>"+resultset.getString("currentstock")+"</td>");
					out.println("<td align='center'><input type='text' name='price"+i+"' id='price"+i+"' value='"+resultset.getString("price")+"' size='4' value='0'></td>");
					out.println("<td align='center'><input type='text' name='newstock"+i+"' id='newstock"+i+"' size='4' value='0'></td>");
					out.println("</tr>");
					out.println("<input type='hidden' name='partid"+i+"' id='partid"+i+"' value='"+resultset.getString("partsid")+"'>");
				}
			}catch(SQLException se){
				out.println(se);
			}
			out.println("</tbody>");
			out.println("</table>");

		}else{

			out.println("<table width='100%'>");
			out.println("<thead>");
			out.println("<tr>");
			out.println("<th >Part Name</th>");
			out.println("<th >Price</th>");
			out.println("<th >Current Stock</th>");
			out.println("<th >Action</th>");
			out.println("</thead>");
			out.println("<tbody>");
			out.println("<tr>");
			out.println("<td align='left'><input type='text' name='partname' id='partname' ></td>");
			out.println("<td align='center'><input type='text' name='price' id='price' size='4' value='0'></td>");
			out.println("<td align='center'><input type='text' name='currentstock' id='currentstock' size='4' value='0'></td>");
			out.println("<td align='left'><input id='button1' name='button' type='button' value='Add Part'  onclick='postForm(\"StockUpdation?button=Add Part\",\"form\")' /></td>");
			out.println("</tr>");
			out.println("</table>");
		}



		out.println("<input type='hidden' name='totrows' id='totrows' value='"+i+"'>");
		if(!button.equals("Add New")){
			out.println("<div align='center'>");
			out.println("	<input id='button1' name='button' type='button' value='Update Stock'  onclick='postForm(\"StockUpdation?button=Update\",\"form\")' />");
			out.println("	<input id='button1' name='button' type='button' value='Add New Part'  onclick='postForm(\"StockUpdation?button=Add New\",\"form\")' />");
			out.println("</div>");
		}
		out.println("<form>");





		out.println("</div>");

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request,response);
	}

}
