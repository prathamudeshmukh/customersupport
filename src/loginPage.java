
import dbComponents.ConnectDB;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;



/**
 * Servlet implementation class loginPage
 */
@WebServlet("/loginPage")
public class loginPage extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public loginPage() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		Connection connection= ConnectDB.newConnection("customersupport");

		
		HttpSession session=request.getSession();
		
		//Statement statement;
		PreparedStatement preparedstatement;
		
		ResultSet resultset;
		String sqlString="",errMsg="";
		String userid	=ConnectDB.getParam(request, "userid");
		String password	=ConnectDB.getParam(request, "password");
		String button	=ConnectDB.getParam(request, "button");

		
		//out.println("This is login pagce!!!");
	
		session.removeAttribute("username");
		session.removeAttribute("userregisterid");
		session.removeAttribute("usertypeid");
		
		
		if(button.equals("Enter")){
			try {
				sqlString="select usertypeid,userregisterid,firstname,userid from userregister where userid=? and password=? ";
				preparedstatement=connection.prepareStatement(sqlString);
				preparedstatement.setString(1, userid);
				preparedstatement.setString(2, password);
				resultset=preparedstatement.executeQuery();
				if(resultset.next()){
					//out.println("user name matched");
					session.setAttribute("username",resultset.getString("firstname"));
					session.setAttribute("userregisterid",resultset.getInt("userregisterid"));
					session.setAttribute("usertypeid",resultset.getInt("usertypeid"));
					
					//request.getRequestDispatcher("mainMenu").forward(request, response);
					response.sendRedirect("mainMenu");
				}else{
					errMsg="User name or password does not match!!!";
					//out.println("user name not matched");
				}
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
				
		out.println("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
		out.println("<html xmlns='http://www.w3.org/1999/xhtml'>");
		out.println("<head>");
		out.println("<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />");
		out.println("<title>Login</title>");
		
		out.println("		<link rel='stylesheet' type='text/css' href='css/theme.css' />");
		out.println("<link rel='stylesheet' type='text/css' href='css/style.css' />");
		out.println("<script>");
		out.println("var StyleFile = 'theme' + document.cookie.charAt(6) + '.css';");
		out.println("document.writeln('<link rel='stylesheet' type='text/css' href='css/' + StyleFile + ''>');");
		out.println("</script>");
		out.println(" <script src='jquery-1.4.2.min.js' > </script>");
		out.println(" <script src='utils.js' > </script>");
		
		out.println("<!--[if IE]>");
		out.println("<link rel='stylesheet' type='text/css' href='css/ie-sucks.css' />");
		out.println("<![endif]-->");
		out.println("</head>");
		out.println("<body>");
		
		out.println("<div id='container'>");
		out.println("<div id='wrapper'>");
		out.println("<div id='content'>");
		out.println("<div id='box'>");
		out.println("<h3 id='adduser'>NapsterManiac Co. Ltd</h3>");
		if(!errMsg.equals("")){
			out.println("<legend>"+errMsg+"</legend>");
		}
		out.println("<form id='form' action='"+ConnectDB.getActionString("loginPage")+"' method='post'>");
		out.println("<fieldset id='personal'>");
		out.println("<legend ><img src='img/icons/key_go.png' title='Login' width='16' height='16' />Login</legend>");
		//out.println("<legend ><a href='#' class='login' />Login</a></legend>");
		out.println("<label for='userid'>User Name</label>"); 
		out.println("<input name='userid' id='userid' type='text' tabindex='1' />");
		out.println("<br />");
		out.println("");	 	                       
		out.println("<label for='pass'>Password</label>");
		out.println("<input name='password' id='pass' type='password' tabindex='2' />");
		out.println("<br />");
				
		out.println("</fieldset>");
		out.println("	<div align='center'>");
		out.println("		<a href='"+ConnectDB.getActionString("RegisterNewUser")+"' >Register new user</a>"); 
		out.println("	</div><br>");
		
		out.println("	<div align='center'>");
		out.println("<input id='button1' name='button' type='submit' value='Enter' />"); 
		out.println("	</div>");
		  
		
		out.println("	 </form>");
		out.println("</div>");
		out.println("</div>");
		
		out.println("		</div>");
		ConnectDB.getHTMLFooter(out,session);
		
		out.println("		</div>");
		
		out.println("</body>");
		out.println("</html>");
		
		try {
			connection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}



	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
			
	}

}
