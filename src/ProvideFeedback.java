

import htmlComponents.HTMLComponents;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dbComponents.ConnectDB;

/**
 * Servlet implementation class ProvideFeedback
 */
@WebServlet("/ProvideFeedback")
public class ProvideFeedback extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProvideFeedback() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		Connection connection= ConnectDB.newConnection("customersupport");
		PreparedStatement preparedstatement;
		
		HttpSession session=request.getSession();
		Statement statement;
		ResultSet resultset;
		String sqlString="";
		
		//String username =(String) session.getAttribute("username");
		//int userregisterid =(Integer) session.getAttribute("userregisterid");
		String button =ConnectDB.getParam(request, "button");
		String feedback=ConnectDB.getParam(request, "feedbackdetails");
		int accepttransid=Integer.parseInt(ConnectDB.getParam(request, "accepttransid"));
		String complaintregisterid=ConnectDB.getParam(request, "complaintregisterid");
		String solutiontransid=ConnectDB.getParam(request, "solutiontransid");
		String feedbackratingid=ConnectDB.getParam(request, "feedbackratingid");
		String solutionratingid=ConnectDB.getParam(request, "solutionratingid");
		
		if(button.equals("FeedBack")){
			try {
				
				sqlString="update solutiontrans set solvedflag=?,feedback=?,feedbackratingid=?,feedbackflag=1,feedbackdate=now(),solutionratingid=? where solutiontransid="+solutiontransid;
				preparedstatement=connection.prepareStatement(sqlString);
				if(feedbackratingid.equals("1") || feedbackratingid.equals("2") ){
					preparedstatement.setInt(1, 1);
				}else{
					preparedstatement.setInt(1, 0);
				}
				preparedstatement.setString(2, feedback);
				preparedstatement.setInt(3, Integer.parseInt(feedbackratingid));
				preparedstatement.setInt(4, Integer.parseInt(solutionratingid));
				preparedstatement.executeUpdate();
				
				sqlString=" update solutiontrans set averagemarks=coalesce(x.avgmarks,0) from ( "+
				" select avg(ratingmarks) avgmarks from solutiontrans st "+
				" inner join solutionrating sr on sr.solutionratingid=st.solutionratingid "+
				" where st.solutiontransid in (select refsolutiontransid from solutiontrans where solutiontransid="+solutiontransid+") or st.refsolutiontransid in (select refsolutiontransid from solutiontrans where solutiontransid="+solutiontransid+") "+
				" ) x where solutiontransid in (select refsolutiontransid from solutiontrans where solutiontransid="+solutiontransid+") ";
				preparedstatement=connection.prepareStatement(sqlString);
				preparedstatement.executeUpdate();
				
				sqlString=" update solutiontrans set averagemarks=coalesce(x.avgmarks,0) from ( "+ 
				"		select avg(ratingmarks) avgmarks from solutiontrans st  "+
				"		inner join solutionrating sr on sr.solutionratingid=st.solutionratingid "+ 
				"		where st.solutiontransid in ("+solutiontransid+") and st.refsolutiontransid is null "+
				"		) x where solutiontransid in ("+solutiontransid+") ";
				preparedstatement=connection.prepareStatement(sqlString);
				preparedstatement.executeUpdate();
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				out.println(e+""+sqlString);
			}
		}
		
		out.println("<div id='box'>");
		out.println("<h3>Provide Feedback</h3>");
		out.println("<form id='form' name='form' action='' method='post'>");
		
		sqlString="select complainttype.complaintdescription, complaintregister.complaintdetails,sat.solution,sat.solutiontransid from complaintregister "+ 
				" inner join complainttype on complainttype.complainttypeid=complaintregister.complainttypeid  "+
				" inner join complaintaccepttrans cat on cat.complaintregisterid=complaintregister.complaintregisterid and cat.solvedflag=0 and cat.forwardflag=0 "+
				" inner join solutiontrans sat on sat.accepttransid=cat.accepttransid and feedbackflag=0 "+ 
				" where complaintregister.complaintregisterid="+complaintregisterid;
		try{
			statement=connection.createStatement();
			resultset=statement.executeQuery(sqlString);
			if(resultset.next()){
				out.println("<fieldset id='a'>");
				out.println("<label for='complainttypeid'>Complaint Type:</label>"+resultset.getString("complaintdescription"));
				out.println("</fieldset >");
				
				out.println("<fieldset id='b'>");
				out.println("<label for='complaintdetails'>Complaint Details:</label>"+resultset.getString("complaintdetails"));
				out.println("</fieldset >");
				
				out.println("<fieldset id='c'>");
				out.print("<label for='solutiondetails'>Solution:</label>");
				out.println("<textarea name='solutiondetails' id='solutiondetails' rows='3' cols='50'>"+resultset.getString("solution")+"</textarea>");
				out.println("<input type='hidden' name='solutiontransid' id='solutiontransid' value='"+resultset.getString("solutiontransid")+"'> ");
				out.println("</fieldset>");
				
			}
		}catch (SQLException e)
		{
			out.println(e);
		}
		

		out.println("<fieldset id='c'>");
		out.println("<label for='solutionratingid'>Solution Rating:</label>"+HTMLComponents.GenerateDropdown("select stars||' '||ratingdescription as rating,solutionratingid from solutionrating order by ratingmarks desc ","solutionratingid", "rating", "solutionratingid", ""));
		out.println("</fieldset >");
		
		out.println("<fieldset id='a'>");
		out.println("<label for='feedbackratingid'>Feedback Rating:</label>"+HTMLComponents.GenerateDropdown("select * from feedbackrating","feedbackratingid", "ratingdescription", "feedbackratingid", ""));
		out.println("</fieldset >");
		
		out.println("<fieldset id='b'>");
		out.println("<label for='feedbackdetails'>Feedback Details:</label>");
		out.println("<textarea name='feedbackdetails' id='feedbackdetails' rows='3' cols='50'></textarea>");
		out.println("</fieldset >");

		out.println("<div align='center'>");
		out.println("<input id='button1' name='button' type='button' value='Submit'  onclick='postForm(\"ProvideFeedback?button=FeedBack\",\"form\")' > ");
		//out.println("<input id='button1' name='button' type='button' value='Send' onclick='x()' /> ");
		out.println("<input id='button2' type='reset' />");
		out.println("</div>");
		out.println("<input type='hidden' name='accepttransid' id='accepttransid' value='"+accepttransid+"'> ");
		out.println("<input type='hidden' name='complaintregisterid' id='complaintregisterid' value='"+complaintregisterid+"'> ");
		out.println("</form>");
		out.println("<h3>Previous Solutions</h3>");
		out.println("<form>");
		
		
		out.println("<table width='100%'><thead><tr>");
		out.println("<th>Solution Date</th>");
		out.println("<th>Solution</th>");
		out.println("</tr></thead>");
		sqlString="select to_char(sat.solutiondate,'dd-mm-yyyy HH:MI') as solutiondate,sat.solution from complaintregister "+ 
		" inner join complaintaccepttrans cat on cat.complaintregisterid=complaintregister.complaintregisterid and cat.solvedflag=0 "+
		" inner join solutiontrans sat on sat.accepttransid=cat.accepttransid and feedbackflag=1 "+ 
		" where complaintregister.complaintregisterid="+complaintregisterid;
		try{
			statement=connection.createStatement();
			resultset=statement.executeQuery(sqlString);
			while(resultset.next()){
				out.println("<td>"+resultset.getString("solutiondate")+"</td>");
				out.println("<td>"+resultset.getString("solution")+"</td>");
			}
		}catch (SQLException e)
		{
			out.println(e);
		}	
		out.println("</table>");
		
		
		
		out.println("</form>");
		out.println("</div>");

	}

	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request,response);
	}
	

}
