

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dbComponents.ConnectDB;

/**
 * Servlet implementation class SolvedComplaintDetails
 */
@WebServlet("/SolvedComplaintDetails")
public class SolvedComplaintDetails extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SolvedComplaintDetails() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		Connection connection= ConnectDB.newConnection("customersupport");
		
		HttpSession session=request.getSession();
		Statement statement;
		ResultSet resultset;
		String sqlString="";
		
		//String username =(String) session.getAttribute("username");
		int userregisterid =(Integer) session.getAttribute("userregisterid");
		String button =ConnectDB.getParam(request, "button");
		String solutiondetails=ConnectDB.getParam(request, "solutiondetails");
		int accepttransid=Integer.parseInt(ConnectDB.getParam(request, "accepttransid"));
		String complaintregisterid=ConnectDB.getParam(request, "complaintregisterid");
		
		
		out.println("<div id='box'>");
		out.println("<h3>Complaint Details</h3>");
		out.println("<form id='form' name='form' action='' method='post'>");
		
		sqlString="select complainttype.complaintdescription, complaintregister.complaintdetails from complaintregister inner join complainttype on complainttype.complainttypeid=complaintregister.complainttypeid where complaintregisterid="+complaintregisterid;
		try{
			statement=connection.createStatement();
			resultset=statement.executeQuery(sqlString);
			if(resultset.next()){
				out.println("<fieldset id='a'>");
				out.println("<label for='complainttypeid'>Complaint Type:</label>"+resultset.getString("complaintdescription"));
				out.println("</fieldset >");
				
				out.println("<fieldset id='b'>");
				out.println("<label for='complaintdetails'>Complaint Details:</label>"+resultset.getString("complaintdetails"));
				out.println("</fieldset >");
			}
		}catch (SQLException e)
		{
			out.println(e+""+sqlString);
		}
		
		
		out.println("<input type='hidden' name='accepttransid' id='accepttransid' value='"+accepttransid+"'> ");
		out.println("<input type='hidden' name='complaintregisterid' id='complaintregisterid' value='"+complaintregisterid+"'> ");
		
		out.println("</form>");
		/*out.println("<fieldset id='d'>");
		out.println("<label for='previousSolution'>Previous Solution</label>");
		out.println("</fieldset >");*/
		
		out.println("<h3>Solutions Provided</h3>");
		out.println("<form>");
		out.println("<table width='100%'><thead><tr>");
		//out.println("<th>Solution Date</th>");
		out.println("<th>Solution</th>");
		out.println("<th>Feedback</th>");
		
		out.println("</tr></thead>");
		sqlString="select sat.feedbackratingid,to_char(sat.solutiondate,'dd-mm-yyyy HH:MI') as solutiondate,sat.solution||'<br>Provided By <b>'||ur.firstname||'</b> on <b>'||to_char(sat.solutiondate,'dd-mm-yyyy HH:MI')||'</b>' as solution,sat.feedback||'<br>On <b>'||to_char(sat.feedbackdate,'dd-mm-yyyy HH:MI')||'</b>' as feedback from complaintregister "+ 
				" inner join complaintaccepttrans cat on cat.complaintregisterid=complaintregister.complaintregisterid "+
				" inner join solutiontrans sat on sat.accepttransid=cat.accepttransid and feedbackflag=1 "+ 
				" inner join userregister ur on ur.userregisterid=sat.solutionby "+
				" where complaintregister.complaintregisterid="+complaintregisterid+
				" order by sat.feedbackdate desc ";
		
		try{
			statement=connection.createStatement();
			resultset=statement.executeQuery(sqlString);
			while(resultset.next()){
				
				out.println("<tr><td>"+resultset.getString("solution")+"</td>");
				if(resultset.getInt("feedbackratingid")==1 || resultset.getInt("feedbackratingid")==2 ){
					out.println("<td><font color='green'>"+resultset.getString("feedback")+"</font></td>");
				}else{
					out.println("<td><font color='red'>"+resultset.getString("feedback")+"</font></td>");
				}
				out.println("</tr>");
				
			}
		}catch (SQLException e)
		{
			out.println(e);
		}
		out.println("</table>");
		
		
		
		
		out.println("</form>");
		out.println("</div>");

	}

	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request,response);
	}
	

}
