

import htmlComponents.HTMLComponents;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dbComponents.ConnectDB;

/**
 * Servlet implementation class AddNewProductModels
 */
@WebServlet("/AddNewProductModels")
public class AddNewProductModels extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddNewProductModels() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		Connection connection= ConnectDB.newConnection("customersupport");

		HttpSession session=request.getSession();
		Statement statement;
		ResultSet resultset;
		String sqlString="",errMsg="";
		PreparedStatement preparedstatement;
		
		String username =(String) session.getAttribute("username");
		int userregisterid =(Integer) session.getAttribute("userregisterid");
		
		
		String productid	=ConnectDB.getParam(request, "productid");
		String modelno	=ConnectDB.getParam(request, "modelno");
		String modelname	=ConnectDB.getParam(request, "modelname");
		String button	=ConnectDB.getParam(request, "button");
		String techspecification	=ConnectDB.getParam(request, "techspecification");
		String productreleasedate=ConnectDB.getParam(request, "productreleasedate");
		
		
		if(button.equals("Add")){
			try {
				sqlString=" INSERT INTO modelsmaster( "+
			            " modelid, modelno, productid, modelname, techspecifications,productreleasedate) "+
						" VALUES ((select coalesce(max(modelid),0)+1 from modelsmaster ), ?, ?, ?, ?,to_date('"+productreleasedate+"','dd-mm-yyyy')); ";

						preparedstatement=connection.prepareStatement(sqlString);
						preparedstatement.setString(1, modelno);
						preparedstatement.setInt(2,Integer.parseInt(productid) );
						preparedstatement.setString(3, modelname);
						preparedstatement.setString(4, techspecification);
						
						preparedstatement.executeUpdate();
			}catch (SQLException e) {
				// TODO Auto-generated catch block
				out.println(e);
			}
		}
		
		
		out.println("<div id='box'>");
		out.println("<h3>Product Models</h3>");
		out.println("<table width='100%'>");
		out.println("<thead>");
		out.println("<tr>");
		out.println("<th >Product</th>");
		out.println("<th >Model No</th>");
		out.println("<th >Model Name</th>");
		out.println("<th >Technical Specifications</th>");
		out.println("</tr>");
		
		out.println("</thead>");
		out.println("<tbody>");
		sqlString="select pm.productname,mm.modelno,mm.modelname,mm.techspecifications as realspecs,coalesce(case when length(mm.techspecifications)>30 then substr(mm.techspecifications,0,30)||'...' else mm.techspecifications end,'') as techspecifications from modelsmaster mm "+
				" inner join productmaster pm on pm.productid=mm.productid " ;
		try{
			statement=connection.createStatement();
			resultset=statement.executeQuery(sqlString);
			while(resultset.next()){
				out.println("<tr>");
				out.println("<td align='left'>"+resultset.getString("productname")+"</td>");
				out.println("<td align='left'>"+resultset.getString("modelno")+"</td>");
				out.println("<td align='left'>"+resultset.getString("modelname")+"</td>");
				out.println("<td align='left' title='"+resultset.getString("realspecs")+"'>"+resultset.getString("techspecifications")+"</td>");
				out.println("</tr>");
			}
		}catch(SQLException se){
			out.println(se);
		}
		
		
		out.println("</tbody>");
		out.println("</table>");

		out.println("</div>");
		
		
		out.println("<div id='box'>");
		out.println("<h3>Add New Models</h3>");
		out.println("<form id='form' method='post'>");
		out.println("<fieldset id='model'>");
		out.println("<legend >Product Model Information :</legend>");
		out.println("<label for='userid'>Product :</label>"); 
		sqlString="select pm.productname,productid from productmaster pm order by productname ";
		out.println(HTMLComponents.GenerateDropdown(sqlString,"productid", "productname", "productid", productid));
		out.println("<br />");                 
		out.println("<label for='firstname'>Model No. :</label>");
		out.println("<input name='modelno' id='modelno' type='text' tabindex='2' />");
		out.println("<br />");
		out.println("<label for='firstname'>Model Name :</label>");
		out.println("<input name='modelname' id='modelname' type='text' tabindex='3' />");
		out.println("<br />");
		out.println("<label for='firstname'>Release Date:</label>");
		out.println("<input name='productreleasedate' id='productreleasedate' type='text' tabindex='3' />");
		out.println("<br />");
		out.println("<label for='firstname'>Technical Specification :</label>");
		out.println("<textarea name='techspecification' id='techspecification' rows='10' cols='15' tabindex='4'  ></textarea>");
		out.println("<br />");
		out.println("</fieldset >");
		
		out.println("<div align='center'>");
		out.println("	<input id='button1' name='button' type='button' value='Add' onclick='postForm(\"AddNewProductModels?button=Add\",\"form\")'/>");
		out.println("	<input id='button1' name='button' type='reset' value='Reset' />"); 
		out.println("</div>");
		
		out.println("</form>");
		out.println("</div>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request,response);
	}

}
