

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dbComponents.ConnectDB;

/**
 * Servlet implementation class PreviousFeedbacks
 */
@WebServlet("/PreviousFeedbacks")
public class PreviousFeedbacks extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PreviousFeedbacks() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		Connection connection= ConnectDB.newConnection("customersupport");

		HttpSession session=request.getSession();
		Statement statement;
		ResultSet resultset;
		String sqlString="";
		
		//String username =(String) session.getAttribute("username");
		int userregisterid =(Integer) session.getAttribute("userregisterid");
		
		out.println("<div id='box'>");
		
		
		boolean rowsreturned=false;

		sqlString="select to_char(st.feedbackdate,'dd-mm-yyyy') as feedbackdate,st.solution,st.feedback,sr.ratingdescription as rating from solutiontrans st "+
		" inner join complaintaccepttrans cat on cat.accepttransid=st.accepttransid "+
		" inner join complaintregister cr on cr.complaintregisterid=cat.complaintregisterid "+
		" inner join solutionrating sr on sr.solutionratingid=st.solutionratingid  "+
		" where cr.complaintby="+userregisterid+" and st.feedbackflag=1";
		try{
			statement=connection.createStatement();
			resultset=statement.executeQuery(sqlString);
			while(resultset.next()){
				rowsreturned=true;
				if(resultset.isFirst()){
					out.println("<h3>Previous Feedbacks</h3>");
					
					out.println("<table width='100%'>");
					out.println("<thead>");
					out.println("<tr>");
					out.println("<th >Feedback Date</th>");
					out.println("<th width='70%'>Solution</th>");
					out.println("<th >Feedback</th>");
					out.println("<th >Rating</th>");
					out.println("</tr>");
					out.println("</thead>");
					out.println("<tbody>");
				}
				
				out.println("<tr>");
				out.println("<td align='center'>"+resultset.getString("feedbackdate")+"</td>");
				out.println("<td>"+resultset.getString("solution")+"</td>");
				out.println("<td>"+resultset.getString("feedback")+"</td>");
				out.println("<td>"+resultset.getString("rating")+"</td>");
				out.println("</tr>");
				
				if(resultset.isLast()){
					out.println("</tbody>");
					out.println("</table>");
				}
				
			}
			
			if(!rowsreturned){
				out.println("<h3 align='center'>");
				out.println("No Previous Feedbacks to display!!");
				out.println("</h3>");
			}
			
			
		}catch(SQLException se){
			out.println(se);
		}
		
		
		

		out.println("</div>");

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request,response);
	}

}
