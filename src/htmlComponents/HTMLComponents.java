package htmlComponents;

import dbComponents.ConnectDB;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class HTMLComponents
 */
@WebServlet("/HTMLComponents")
public class HTMLComponents {
	public static void main(String[] argv) {
	
	}
	
	public static String GenerateDropdown(String sqlString,String name,String label,String value,String defaultvalue){
		PreparedStatement preparedstatement;
		ResultSet resultset;
		String returnString="";
		Connection connection= ConnectDB.newConnection("customersupport");		
		
		try {
			
			preparedstatement=connection.prepareStatement(sqlString);
			resultset=preparedstatement.executeQuery();
			returnString=" <select name='"+name+"' id='"+name+"' > ";
			while(resultset.next()){

				returnString+="     <option value='"+resultset.getString(value)+"'   "+(resultset.getString(value).equals(defaultvalue)?"selected":"")+" >"+resultset.getString(label)+"</option>";
				
			}
			returnString+="  </select>";
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return returnString;
		
	}

}
