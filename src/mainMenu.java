
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dbComponents.ConnectDB;

/**
 * Servlet implementation class mainMenu
 */
@WebServlet("/mainMenu")
public class mainMenu extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public mainMenu() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		Connection connection= ConnectDB.newConnection("customersupport");

		
		HttpSession session=request.getSession();
		
		Statement statement;
		PreparedStatement preparedstatement;
		
		ResultSet resultset,resultset1;
		String sqlString="",errMsg="";
		String prevsubmenu="";
		
		String username =(String) session.getAttribute("username");
		int usertypeid =(Integer) session.getAttribute("usertypeid");
		int userregisterid=(Integer) session.getAttribute("userregisterid");
		

		
		out.println(" <!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>  ");
		out.println(" <html xmlns='http://www.w3.org/1999/xhtml'> ");
		out.println(" <head> ");
		out.println(" <meta http-equiv='Content-Type' content='text/html; charset=utf-8' /> ");
		out.println(" <title>Welcome "+username+"</title> ");
		out.println(" <link rel='stylesheet' type='text/css' href='css/theme.css' /> ");
		out.println(" <link rel='stylesheet' type='text/css' href='css/style.css' /> ");
		out.println(" <script> ");
		out.println("    var StyleFile = 'theme' + document.cookie.charAt(6) + '.css'; ");
		out.println("    document.writeln(\"<link rel='stylesheet' type='text/css' href='css\"+StyleFile+\"'>\"); ");
		out.println(" </script> ");
		out.println(" <script src='jquery-1.4.2.min.js' > </script>");
		out.println(" <script src='utils.js' > </script>");
		out.println(" <!--[if IE]> ");
		out.println(" <link rel='stylesheet' type='text/css' href='css/ie-sucks.css' /> ");
		out.println(" <![endif]--> ");
		out.println(" </head> ");

		out.println(" <body> ");
		out.println(" 	<div id='container'> ");
		
		out.println("     	<div id='header'> ");
		out.println("         	<h2>NapsterManiac Co. Ltd.</h2> ");
		//out.println("         	<h3>Sign Out</h3> ");
		out.println("     		<div id='topmenu'> ");
		out.println("             	<ul> ");
		sqlString=" select distinct m.menusequence,m.mainmenudescription from mainmenu m "+ 
		" inner join submenu sb on sb.mainmenuid=m.mainmenuid  "+
		" inner join programregister p on p.submenuid=sb.submenuid "+ 
		" inner join userrolesdefination d on d.userroleid=p.userroleid "+ 
		" where d.usertypeid="+usertypeid+
		" order by m.menusequence ";
		try{
			statement=connection.createStatement();
			resultset=statement.executeQuery(sqlString);
			while(resultset.next()){
				out.println("<li ><a href='#'>"+resultset.getString("mainmenudescription")+"</a></li> ");
			}
		}catch(SQLException ex){
			
		}
		out.println(" 				</ul> ");
		out.println("     		</div> ");
		out.println("       </div> ");
		

		out.println("<div id='wrapper'> ");
		
		out.println("<div id='content'> ");
		
		sqlString=" select userroleid from userrolesdefination where usertypeid="+usertypeid;
		try{
			statement=connection.createStatement();
			resultset=statement.executeQuery(sqlString);
			while(resultset.next()){
				if(resultset.getInt("userroleid")==4){
					sqlString="select 'You have '||(select count(*) from complaintaccepttrans at "+
					" left join solutiontrans sat on sat.accepttransid=at.accepttransid "+
					" where at.acceptedby=ur.userregisterid and at.solvedflag=0 and forwardflag=0 and sat.accepttransid is null "+
					" )||' new complaints,'||(select count(*) from solutiontrans "+ 
					" where solutionby=ur.userregisterid and to_char(solutiondate,'yyyy-mm-dd')=to_char(now(),'yyyy-mm-dd') and feedbackratingid not in (1,2))||' negative feedbacks and '||( "+
					" select count(*) from solutiontrans "+ 
					" where solutionby=ur.userregisterid and to_char(solutiondate,'yyyy-mm-dd')=to_char(now(),'yyyy-mm-dd') "+ 
					" )||' solutions provided today' from userregister ur "+
					" where userregisterid="+userregisterid;

					sqlString=" select 'Today, you have '||newcomp||' new complaints,'||negativfeedbacks||' negative feedbacks, '||totsolution||' solutions provided and efficiency is '||trunc(((newcomp/case when totsolution=0 then 1 else totsolution end )*100)::numeric,2) from ( "+
					" select (select count(*) from complaintaccepttrans at left join solutiontrans sat on sat.accepttransid=at.accepttransid where at.acceptedby=ur.userregisterid and at.solvedflag=0 and forwardflag=0 and sat.accepttransid is null )::numeric as newcomp, "+
					" (select count(*) from solutiontrans where solutionby=ur.userregisterid and to_char(feedbackdate,'yyyy-mm-dd')=to_char(now(),'yyyy-mm-dd') and feedbackratingid not in (1,2)) as negativfeedbacks, "+
					" (select count(*) from solutiontrans where solutionby=ur.userregisterid and to_char(solutiondate,'yyyy-mm-dd')=to_char(now(),'yyyy-mm-dd') )::numeric as totsolution from userregister ur 	"+
					" where userregisterid="+userregisterid+
					" ) x ";
					//out.println(sqlString);
					
					resultset1=statement.executeQuery(sqlString);
					while(resultset1.next()){
						out.println("<div id='rightnow'>");
						out.println("<h3 class='reallynow'>");
						out.println("<span>Right Now</span>");
						out.println("<br />");
						out.println("</h3>");
						out.println("<p class='youhave'>"+resultset1.getString(1)+"</p>");
						out.println("</div>");
					}
		
				}
					
			}
		}catch(SQLException ex){
			
		}
		out.println("</div> ");
		
		
		out.println("<div id='sidebar'> ");
		
		
		out.println(" <ul> ");
		out.println("  <li>");
		sqlString=" select m.mainmenudescription,submenudescription,servletname,m.classname,sb.classname as subclass from mainmenu m "+
		" inner join submenu sb on sb.mainmenuid=m.mainmenuid "+
		" inner join programregister p on p.submenuid=sb.submenuid "+
		" inner join userrolesdefination d on d.userroleid=p.userroleid "+
		" where d.usertypeid="+usertypeid+" "+
		" order by m.menusequence,sb.submenusequence ";
		//out.println(sqlString);
		try{
		statement=connection.createStatement();
		resultset=statement.executeQuery(sqlString);
		while(resultset.next()){
						
			if(!resultset.getString("mainmenudescription").equals(prevsubmenu)){
				if(!resultset.isFirst()){
					out.println("  </ul> ");
				}
				out.println("  <h3><a href='#' class='"+resultset.getString("classname")+"'>"+resultset.getString("mainmenudescription")+"</a></h3> ");
				out.println("  <ul> ");
			}
			
			out.println("  <li><a href='#' onclick='getMainPage(\""+resultset.getString("servletname")+"\")' class='"+resultset.getString("subclass")+"'>"+resultset.getString("submenudescription")+"</a></li> ");
			
			
			prevsubmenu=resultset.getString("mainmenudescription");
		}
		
		}catch(SQLException ex){
			
		}
		out.println("  </ul> ");
		out.println("  </li> ");
		
		out.println(" </ul>        ");
						
						
		out.println("           </div> ");
		out.println("       </div> ");
		ConnectDB.getHTMLFooter(out, session);
		out.println(" </div> ");
		out.println(" </body> ");
		out.println(" </html> ");

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
