

import htmlComponents.HTMLComponents;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dbComponents.ConnectDB;

/**
 * Servlet implementation class AddNewUser
 */
@WebServlet("/AddNewUser")
public class AddNewUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddNewUser() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		Connection connection= ConnectDB.newConnection("customersupport");

		HttpSession session=request.getSession();
		Statement statement;
		ResultSet resultset,resultset1;
		String errMsg="",succMsg="",validMsg="",sqlString="";
		int userregisterid=0;
		String button	=ConnectDB.getParam(request, "button");
		
		String firstname	=ConnectDB.getParam(request, "firstname");
		String lastname	=ConnectDB.getParam(request, "lastname");
		String emailid	=ConnectDB.getParam(request, "emailid");
		String password	=ConnectDB.getParam(request, "password");
		String streetaddress	=ConnectDB.getParam(request, "streetaddress");
		String cityid	=ConnectDB.getParam(request, "cityid");
		String countryid	=ConnectDB.getParam(request, "countryid");
		String stateid	=ConnectDB.getParam(request, "stateid");
		String telephone	=ConnectDB.getParam(request, "telephone");
		String usertypeid=ConnectDB.getParam(request, "usertypeid");
		String repassword=ConnectDB.getParam(request, "repassword");
		PreparedStatement preparedstatement;
		
		//String username =(String) session.getAttribute("username");
		//int userregisterid =(Integer) session.getAttribute("userregisterid");
		
		//out.println("<div id='box'>");
		
		if(button.equals("Add")){
			try {
				statement=connection.createStatement();
				resultset=statement.executeQuery("select coalesce(max(userregisterid),0)+1 from  userregister");
				if(resultset.next()){
					userregisterid=resultset.getInt(1);
				}
					if(firstname.trim().equals("") || lastname.trim().equals("") ||emailid.trim().equals("") ||password.trim().equals("") || password.length()<8 || !repassword.equals(password) ||
							streetaddress.trim().equals("") ||cityid.trim().equals("") ||countryid.trim().equals("") ||stateid.trim().equals("") 
							||telephone.trim().equals("")||usertypeid.trim().equals("") || !isNumeric(telephone.trim()) || isNumeric(firstname.trim()) || isNumeric(lastname.trim()) ||telephone.trim().length()>10 ){
						if(usertypeid.trim().equals("")){
							validMsg="Enter User Type";	
						}else if(lastname.trim().equals("")){
							validMsg="Enter the last name";	
						}else if(firstname.trim().equals("")){
							validMsg="Enter the first name";	
						}else if(emailid.trim().equals("")){
							validMsg="Enter the E-mail ID";	
						}else if(password.trim().equals("")){
							validMsg="Enter password";	
						}else if( password.length()<8){
							validMsg="Password should be more than 8 letters";
						}else if(!repassword.equals(password)){
							validMsg="re Enter the correct password";	
						}else if(streetaddress.trim().equals("")){
							validMsg="Enter Street Address";	
						}else if(countryid.trim().equals("")){
							validMsg="Enter Country";	
						}else if(stateid.trim().equals("")){
							validMsg="Enter State";	
						}else if(cityid.trim().equals("")){
							validMsg="Enter City";	
						}else if(telephone.trim().equals("")){
							validMsg="Enter Telephone";	
						}else if(!isNumeric(telephone.trim())){
							validMsg="Enter Telephone no in digits";
						}else if(telephone.trim().length()>10){
							validMsg="Telephone no should be less than 10 digits";
						}else if(isNumeric(lastname.trim())){
							validMsg="Enter valid last name";
						}else if(isNumeric(firstname.trim())){
							validMsg="Enter valid first name";
						}
						
						
					}else{
								
							//out.println("user name matched");
							resultset1=statement.executeQuery("select * from  userregister where firstname='"+firstname+"' ");
							if(!resultset1.next()){
								
							sqlString=" INSERT INTO userregister(userregisterid, userid, firstname, lastname, emailid,usertypeid, " +
									"registerationdate, password, streetaddress, cityid,countryid, stateid, telephone) "+
									" VALUES (?, ?, ?, ?, ?, ?, now(), ?, ?, ?, ?, ?, ?); ";
									preparedstatement=connection.prepareStatement(sqlString);
									preparedstatement.setInt(1, userregisterid);
									preparedstatement.setString(2, firstname.toLowerCase());
									preparedstatement.setString(3, firstname.toUpperCase());
									preparedstatement.setString(4, lastname.toUpperCase());
									preparedstatement.setString(5, emailid.toLowerCase());
									preparedstatement.setInt(6, Integer.parseInt(usertypeid));
									preparedstatement.setString(7, password);
									preparedstatement.setString(8, streetaddress);
									preparedstatement.setInt(9, Integer.parseInt(cityid));
									preparedstatement.setInt(10, Integer.parseInt(countryid));
									preparedstatement.setInt(11, Integer.parseInt(stateid));
									preparedstatement.setString(12, telephone);
									
									preparedstatement.executeUpdate();
		
									succMsg="User succesfully added !";
							}else{
								errMsg="User name already registered !";
							}
						}
							
				
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if(!errMsg.equals("")){
			out.println("<div class='error'>"+errMsg+"</div>");
		}
		if(!succMsg.equals("")){
			out.println("<div class='success'>"+succMsg+"</div>");
		}
		if(!validMsg.equals("")){
			out.println("<div class='validation'>"+validMsg+"</div>");
		}
		out.println("<div id='box'>");
		out.println("<h3 id='adduser'>Add New User</h3>");
		
		
		//}
		out.println("<form id='form' action='"+ConnectDB.getActionString("AddNewUser")+"' method='post'>");
		out.println("<fieldset id='personal'>");
		out.println("<legend >Personal Information </legend>");
		//out.println("<legend ><a href='#' class='login' />Login</a></legend>");
		out.println("<label for='usertypeid'>User Type :</label>");
		sqlString="select typedescription,usertypeid from usertype where usertypeid not in (1,2) order by usertypeid";
		out.println(HTMLComponents.GenerateDropdown(sqlString,"usertypeid", "typedescription", "usertypeid", usertypeid));
		out.println("<br />");
		out.println("<label for='userid'>Last Name :</label>"); 
		out.println("<input name='lastname' id='lastname' type='text' tabindex='1' value='"+lastname+"'/>");
		out.println("<br />");                 
		out.println("<label for='firstname'>First name :</label>");
		out.println("<input name='firstname' id='firstname' type='text' tabindex='2'  value='"+firstname+"'/>");
		out.println("<br />");
		out.println("<label for='emailid'>Email Id :</label>"); 
		out.println("<input name='emailid' id='emailid' type='text' tabindex='3'  value='"+emailid+"'/>");
		out.println("<br />");	 	                       
		out.println("<label for='password'>Password :</label>");
		out.println("<input name='password' id='password' type='password' tabindex='4' />");
		out.println("<br />");
		out.println("<label for='repassword'>Re-Enter Password :</label>");
		out.println("<input name='repassword' id='repassword' type='password' tabindex='5' onblur='passwordcheck()'/>");
		
		out.println("</fieldset>");
		
		out.println("<fieldset id='address'>");
		out.println("<legend >Contact Information </legend>");
		//out.println("<legend ><a href='#' class='login' />Login</a></legend>");
		
		out.println("<fieldset >");
		out.println("<label for='streetaddress'>Street address :</label>"); 
		out.println("<input name='streetaddress' id='streetaddress' size='35' type='text' tabindex='6'  value='"+streetaddress+"'/>");
		out.println("<br />");   
		out.println("</fieldset >");
		
		out.println("<fieldset >");
		out.println("<label for='country'>Country :</label>"); 
		sqlString="select countryname,countryid from countrymaster order by countryname ";
		out.println(HTMLComponents.GenerateDropdown(sqlString,"countryid' onchange='calldropdown(\"countryid\",\"stateid\",\"sqlstate\")", "countryname", "countryid", countryid));
		//out.println("<input name='country' id='country' type='text' tabindex='8'  value='"+country+"' />");
		out.println("<br />");	 	                       
		out.println("</fieldset >");
		
		out.println("<fieldset >");
		out.println("<label for='state'>State :</label>");
		out.println("	<input id='sqlstate' name='sqlstate' type='hidden' value='select stateid,statename from statemaster where countryid=? order by statename' />"); 
		sqlString="select statename,stateid from statemaster order by statename limit 20 ";
		out.println(HTMLComponents.GenerateDropdown(sqlString,"stateid' onchange='calldropdown(\"stateid\",\"cityid\",\"sqlcity\")", "statename", "stateid", stateid));
		out.println("<br />");
		out.println("</fieldset >");
		
		out.println("<fieldset >");
		out.println("<label for='city'>City :</label>");
		out.println("	<input id='sqlcity' name='sqlcity' type='hidden' value='select cityid,cityname from citymaster where stateid=? order by cityname' />");
		sqlString="select cityname,cityid from citymaster  order by cityname limit 20 ";
		out.println(HTMLComponents.GenerateDropdown(sqlString,"cityid", "cityname", "cityid", cityid));
		out.println("<br />");
		out.println("</fieldset >");
		
		out.println("<fieldset >");
		out.println("<label for='telephone'>Telephone :</label>");
		out.println("<input name='telephone' id='telephone' type='text' tabindex='10' value='"+telephone+"'/>");
		out.println("</fieldset>");
		out.println("</fieldset >");
		
		
		
		out.println("<div align='center'>");
		out.println("	<input id='button1' name='button' type='button' value='Add'  onclick='postForm(\"AddNewUser?button=Add\",\"form\")' />");
		out.println("	<input id='button1' name='button' type='reset' value='Reset' />"); 
		//out.println("	<a href='"+ConnectDB.getActionString("loginPage")+"'><input id='button1' name='button' type='button' value='Cancel' /></a>");
		out.println("</div>");
		
		out.println("	 </form>");
		out.println("</div>");
		

	}

	public static boolean isNumeric(String s){
		for (char c :s.toCharArray()){
			if(Character.isDigit(c)) return true;
		}
		return false;
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request,response);
	}

}
