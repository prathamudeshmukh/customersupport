

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dbComponents.ConnectDB;

/**
 * Servlet implementation class SearchSolution
 */
@WebServlet("/SearchSolution")
public class SearchSolution extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchSolution() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		Connection connection= ConnectDB.newConnection("customersupport");
		PreparedStatement preparedstatement;
		
		HttpSession session=request.getSession();
		Statement statement;
		ResultSet resultset;
		String sqlString="";
		
		//String username =(String) session.getAttribute("username");
		/*int userregisterid =(Integer) session.getAttribute("userregisterid");
		int usertypeid =(Integer) session.getAttribute("usertypeid");
		String button =ConnectDB.getParam(request, "button");
		String solutiondetails=ConnectDB.getParam(request, "solutiondetails");
		int accepttransid=Integer.parseInt(ConnectDB.getParam(request, "accepttransid"));
		*/
		String searchstr=ConnectDB.getParam(request, "searchstr");
		String modelid=ConnectDB.getParam(request, "modelid");
		
		
		
		sqlString="select * from ( "+
		" select distinct on (cr.complaintregisterid) cr.complaintregisterid,m.modelname,complaintdetails,st.solution||'<br>Provided By <b>'||ur.firstname||'</b> on <b>'||to_char(st.solutiondate,'dd-mm-yyyy HH:MI')||'</b>' as solution,count(tag.*) as cnt,array_agg(originalword) as tags from complaintregister cr "+
		" inner join tagwordsallocation tag on tag.complaintregisterid=cr.complaintregisterid "+
		" inner join technicalwords tw on tw.wordid=tag.wordid "+
		" inner join complaintaccepttrans ct on ct.complaintregisterid=cr.complaintregisterid and ct.solvedflag=1 "+
		" inner join solutiontrans st on st.accepttransid=ct.accepttransid and st.solvedflag=1 "+
		" inner join userregister ur on ur.userregisterid=st.solutionby "+
		" inner join modelsmaster m on m.modelid=cr.modelid "+
		" where cr.solvedflag =1 and tw.word in ( "+
		"		select distinct on(sentence)  word from ( "+ 
		"		select * from ( "+ 
		"		select word,wordid,lower(unnest(string_to_array(replace(replace(?,'.',' '),',',' '),' '))) as sentence  from technicalwords "+ 
		"		) x "+ 
		"		where checkword(sentence,word) "+ 
		"		order by word "+  
		"		) y "+
		" )  and  m.modelid in ("+modelid+") and st.refsolutiontransid is null "+ 
		" group by cr.complaintregisterid,m.modelname,complaintdetails,st.solution,ur.firstname,st.solutiondate "+
		" ) x "+
		" order by cnt desc ";
		try{
			preparedstatement=connection.prepareStatement(sqlString);
			preparedstatement.setString(1, searchstr);
			resultset=preparedstatement.executeQuery();
			while(resultset.next()){
				//
				if(resultset.isFirst()){
					out.println("<h3>Suggested solutions</h3>");
					out.println("<form>");
					out.println("<table width='100%'><thead><tr>");
					out.println("<th>Model</th>");
					out.println("<th>Complaint</th>");
					out.println("<th>Solution</th>");
					out.println("<th>Tags</th>");
					out.println("</tr></thead>");
				}
				
				out.println("<tr><td>"+resultset.getString("modelname")+"</td>");
				out.println("<td>"+resultset.getString("complaintdetails")+"</td>");
				out.println("<td>"+resultset.getString("solution")+"</td>");
				out.println("<td>"+resultset.getString("tags")+"</td></tr>");
				
				if(resultset.isLast()){
					out.println("</table>");
					out.println("</form>");
				}

			}
		}catch (SQLException e)
		{
			out.println(e);
		}

		
	}

	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request,response);
	}
	

}
