

import htmlComponents.HTMLComponents;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dbComponents.ConnectDB;

/**
 * Servlet implementation class RegisterNewProduct
 */
@WebServlet("/RegisterNewProduct")
public class RegisterNewProduct extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegisterNewProduct() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		Connection connection= ConnectDB.newConnection("customersupport");

		HttpSession session=request.getSession();
		Statement statement;
		ResultSet resultset;
		String errMsg="",sqlString="";
		//int userregisterid=0;
		String button	=ConnectDB.getParam(request, "button");
		String modelid	=ConnectDB.getParam(request, "modelid");
		String serialno	=ConnectDB.getParam(request, "serialno");
		int userregisterid =(Integer) session.getAttribute("userregisterid");

		PreparedStatement preparedstatement;
		

		
		if(button.equals("Register")){
			try {
				
				sqlString="select machinesaleid from machinesale where modelid=? and serialno=? and registrationid is null ";
				preparedstatement=connection.prepareStatement(sqlString);
				preparedstatement.setInt(1, Integer.parseInt(modelid));
				preparedstatement.setString(2, serialno);
				resultset=preparedstatement.executeQuery();
				if(resultset.next()){
					//out.println("user name matched");
					
							sqlString=" update machinesale set registrationid=? where machinesaleid=? ";
									preparedstatement=connection.prepareStatement(sqlString);
									preparedstatement.setInt(1, userregisterid);
									preparedstatement.setInt(2, resultset.getInt("machinesaleid"));
									preparedstatement.executeUpdate();
					
				}else{
					errMsg="User for this Serial No is already registered !";
					//out.println("user name not matched");
				}
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
		//out.println("<h3 id='adduser'>NapsterManiac Co. Ltd</h3>");
		if(!errMsg.equals("")){
			out.println("<div><font color='red'><b>"+errMsg+"</b></font></div>");
		}
		out.println("<div id='box'>");
		out.println("    <h3 id='adduser'>Register New Product</h3>");
		out.println("<form id='form' action='"+ConnectDB.getActionString("RegisterNewProduct")+"' method='post'>");
		out.println("<fieldset id='productinfo'>");
		//out.println("<legend >Product Information :</legend>");
		out.println("<label for='modelid'>Model No. :</label>");
		sqlString="select modelno||'-'||modelname as modelname,modelid from modelsmaster order by modelno ";
		out.println(HTMLComponents.GenerateDropdown(sqlString,"modelid", "modelname", "modelid", modelid));
		out.println("<br />");	 	      
		out.println("</fieldset>");
		out.println("<fieldset>");
		out.println("<label for='state'>Serial No. :</label>");
		out.println("<input name='serialno' id='serialno' type='text' size='30' tabindex='11' value='"+serialno+"'/>");
		out.println("</fieldset>");
		
		
		out.println("<div align='center'>");
		out.println("	<input id='button1' name='button' type='button' value='Register' onclick='postForm(\"RegisterNewProduct?button=Register\",\"form\")'/>");
		out.println("	<input id='button1' name='button' type='reset' value='Reset' />"); 
		out.println("</div>");
		
		out.println("	 </form>");
		out.println("	 </div>");
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request,response);
	}

}
