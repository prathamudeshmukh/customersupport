/*
 * This class is part of the book "iText in Action - 2nd Edition"
 * written by Bruno Lowagie (ISBN: 9781935182610)
 * For more info, go to: http://itextpdf.com/examples/
 * This example only works with the AGPL version of iText.
 */

//package part3.chapter09;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import dbComponents.ConnectDB;

@WebServlet("/TotalProblemsSolved")
public class TotalProblemsSolved extends HttpServlet {

    /**
     * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        try {
            // Get the text that will be added to the PDF
        	
    		Connection connection= ConnectDB.newConnection("customersupport");
    		//PreparedStatement preparedstatement;
    		
    		HttpSession session=request.getSession();
    		Statement statement;
    		ResultSet resultset;
    		String sqlString="";
    		String errMsg="",succMsg="",validMsg="";
    		
    		//String username =(String) session.getAttribute("username");
    		int userregisterid =(Integer) session.getAttribute("userregisterid");
    		int usertypeid =(Integer) session.getAttribute("usertypeid");
    		String mode =ConnectDB.getParam(request, "mode");
    		String fromdate=ConnectDB.getParam(request, "fromdate");
    		
    		
            if(mode.equals("")){
            	
            	response.setContentType("text/html");
        		PrintWriter out = response.getWriter();
        		
            	out.println("<div id='box'>");
        		out.println("<h3>Total Problems Solved</h3>");
        		out.println("<form id='form' name='form' action='' method='post'>");
        		out.println("<fieldset id='c'>");
				out.println("<label for='product'>From Date:</label>");
				out.println("<input name='fromdate' id='fromdate' type='text' tabindex='1' />(dd-mm-yyyy)");
				out.println("</fieldset >");
				out.println("<div align='center'>");
				//out.println("<input id='button1' name='button' type='button' value='Submit'  onclick='postForm(\"ComplaintAcceptance?button=Send\",\"form\",\"Are you sure to provide this solution?\")' > ");
				out.println("<input id='button1' name='button' type='button' value='Report'  onclick='openpopup(\"TotalProblemsSolved?mode=1&fromdate=$1\",\"fromdate\")' >");
				out.println("<input id='button2' type='reset' />");
				out.println("</div>");
        		out.println("</form>");
        		out.println("</div>");

            }else{
	            	
            		response.setHeader("Expires", "0");
		            response.setHeader("Cache-Control","must-revalidate, post-check=0, pre-check=0");
		            response.setHeader("Pragma", "public");
		            response.setContentType("application/pdf");
		            Document document = new Document();
		            ByteArrayOutputStream baos = new ByteArrayOutputStream();
		            PdfWriter.getInstance(document, baos);
		            Font fontbold = FontFactory.getFont("Times-Roman", 12, Font.BOLD);
		            Font fontnormal = FontFactory.getFont("HELVETICA", 10, Font.NORMAL);
		            
		            PdfPTable table1 = new PdfPTable(4);
		            table1.setWidths(new int[]{40, 20, 20,20});
		            table1.setWidthPercentage(100f);
		            PdfPCell cell;
		            
		           
		            table1.addCell(createCell("Total Problems Solved Report from "+fromdate,fontbold,Element.ALIGN_CENTER,4));
		            
		            table1.addCell(createCell("Complaint Type",fontbold,Element.ALIGN_CENTER,1));
		            table1.addCell(createCell("Total Complaints",fontbold,Element.ALIGN_CENTER,1));
		            table1.addCell(createCell("Solved Complaints",fontbold,Element.ALIGN_CENTER,1));
		            table1.addCell(createCell("In Process",fontbold,Element.ALIGN_CENTER,1));
		            try {
		                sqlString="select cr.complainttypeid,ct.complaintdescription,count(*) as totalcomplaints,(select count(*) from complaintregister where solvedflag=1 and complaintregister.complainttypeid=cr.complainttypeid and complaintregister.complaintdate::date >='"+fromdate+"'::date) as solvedcomp,(select count(complaintregister .*) from complaintregister where complaintregister.solvedflag=0 and complaintregister.complainttypeid=cr.complainttypeid and complaintregister.complaintregisterid  in (select complaintregisterid  from complaintaccepttrans where solvedflag=0) and complaintregister.complaintdate::date >='"+fromdate+"'::date) as inprocess from complaintregister cr "+
						" inner join complainttype ct on ct.complainttypeid=cr.complainttypeid "+
						" where cr.complaintdate >=to_date('"+fromdate+"','dd-mm-yyyy') "+
						" group by cr.complainttypeid,ct.complaintdescription "+
						" order by ct.complaintdescription ";
		    			statement=connection.createStatement();
		    			resultset=statement.executeQuery(sqlString);
		    			while(resultset.next())
		    			{
				            table1.addCell(createCell(resultset.getString("complaintdescription"),fontnormal,Element.ALIGN_LEFT,1));
				            table1.addCell(createCell(resultset.getString("totalcomplaints"),fontnormal,Element.ALIGN_CENTER,1));
				            table1.addCell(createCell(resultset.getString("solvedcomp"),fontnormal,Element.ALIGN_CENTER,1));
				            table1.addCell(createCell(resultset.getString("inprocess"),fontnormal,Element.ALIGN_CENTER,1));
		    			}
		    			
		            } catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		            
		            document.open();
		            
		            document.add(table1);
		            
		            
		            document.close();
		            
		            // setting some response headers
		            
		            response.setContentLength(baos.size());
		            // write ByteArrayOutputStream to the ServletOutputStream
		            OutputStream os = response.getOutputStream();
		            baos.writeTo(os);
		            os.flush();
		            os.close();
            }
        }
        catch(DocumentException e) {
            throw new IOException(e.getMessage());
        }
    }
    public PdfPCell createCell(String displaystr,Font font,int align,int colspan){
    	PdfPCell cell = new PdfPCell(new Phrase(displaystr,font));
    	cell.setColspan(colspan);
    	cell.setHorizontalAlignment(align);
    	//cell.setVerticalAlignment(Element.ALIGN_CENTER);
    	return cell;
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request,response);
	}
	
    /**
     * Serial version UID.
     */
    private static final long serialVersionUID = 6067021675155015602L;

}
