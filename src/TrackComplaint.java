

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dbComponents.ConnectDB;

/**
 * Servlet implementation class TrackComplaint
 */
@WebServlet("/TrackComplaint")
public class TrackComplaint extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TrackComplaint() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		Connection connection= ConnectDB.newConnection("customersupport");

		HttpSession session=request.getSession();
		Statement statement;
		ResultSet resultset;
		String sqlString="",errMsg="";
		
		String username =(String) session.getAttribute("username");
		int userregisterid =(Integer) session.getAttribute("userregisterid");
		
		out.println("<div id='box'>");
		out.println("<h3>Track Complaint</h3>");
		out.println("<table width='100%'>");
		out.println("<thead>");
		out.println("<tr>");
		out.println("<th >Complaint No#</th>");
		out.println("<th >Complaint Type</th>");
		out.println("<th width='40%'>Complaint</th>");
		out.println("<th >Status</th>");
		out.println("</tr>");
		
		out.println("</thead>");
		out.println("<tbody>");
		sqlString="select complaintdescription,complaintdetails,st.accepttransid,cat.complaintregisterid,case when coalesce(stx.present,0) =0 then 'Pending with '||p.firstname else case when st.accepttransid is not null then 'Provide Feedback' else '' end end as status from complaintregister cr "+ 
		" inner join complaintaccepttrans cat on cat.complaintregisterid=cr.complaintregisterid and cat.solvedflag=0 and cat.forwardflag=0 "+ 
		" left join solutiontrans st on st.accepttransid=cat.accepttransid and st.feedbackflag=0  "+
		" left join ( "+
		" select accepttransid,case when count(*) >0 then 1 else 0 end as present from solutiontrans "+ 
		" group by accepttransid "+
		" )stx on stx.accepttransid=cat.accepttransid "+ 
		" left join userregister p on p.userregisterid=cat.acceptedby "+
		" inner join complainttype ct on ct.complainttypeid=cr.complainttypeid "+ 
		" where cr.complaintby="+userregisterid;
		try{
			statement=connection.createStatement();
			resultset=statement.executeQuery(sqlString);
			while(resultset.next()){
				out.println("<tr>");
				out.println("<td align='center'>"+resultset.getString("complaintregisterid")+"</td>");
				out.println("<td align='center'>"+resultset.getString("complaintdescription")+"</td>");
				out.println("<td>"+resultset.getString("complaintdetails")+"</td>");
				out.println("<td align='center'>");
				if(resultset.getString("status").equals("Provide Feedback")){
					out.println("<a href='#' onclick='getMainPage(\"ProvideFeedback?accepttransid="+resultset.getString("accepttransid")+"&complaintregisterid="+resultset.getString("complaintregisterid")+"\")'>"+resultset.getString("status")+"</a>");
				}else{
					out.println(resultset.getString("status"));
				}
				out.println("</td>");
				out.println("</tr>");
			}
		}catch(SQLException se){
			out.println(se);
		}
		
		
		out.println("</tbody>");
		out.println("</table>");

		out.println("</div>");
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request,response);
	}

}
