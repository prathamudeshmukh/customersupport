

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dbComponents.ConnectDB;

/**
 * Servlet implementation class MyProduct
 */
@WebServlet("/MyProduct")
public class MyProduct extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MyProduct() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		Connection connection= ConnectDB.newConnection("customersupport");

		HttpSession session=request.getSession();
		Statement statement;
		ResultSet resultset;
		String sqlString="";
		
		//String username =(String) session.getAttribute("username");
		int userregisterid =(Integer) session.getAttribute("userregisterid");
		
		out.println("<div id='box'>");
		
		
		boolean rowsreturned=false;
		out.println("<h3>My Products</h3>");
		
		out.println("<table width='100%'>");
		out.println("<thead>");
		out.println("<tr>");
		out.println("<th >Product</th>");
		out.println("<th >Model Name/No</th>");
		out.println("<th >Purchase Date</th>");
		out.println("<th >Warranty Period</th>");
		out.println("<th >Warranty Expiry Date</th>");
		out.println("<th >Complaints Registered</th>");
		out.println("</tr>");
		out.println("</thead>");
		out.println("<tbody>");

		sqlString="select mm.modelid,pm.productname,mm.modelname,mm.modelno,to_char(saledate,'dd-mm-yyyy') as saledate,warrantyperiod,case when warrantyexpirydate<now()::date then '<font color=red>'||to_char(warrantyexpirydate,'dd-mm-yyyy')||'</font>' else '<font color=green>'||to_char(warrantyexpirydate,'dd-mm-yyyy')||'</font>'  end as warrantyexpirydate," +
				" (select count(*) from complaintregister cr where cr.modelid=ms.modelid) as complaintsregistered "+
				" from machinesale ms "+
				" inner join userregister ur on ur.userregisterid=ms.registrationid "+
				" inner join modelsmaster mm on mm.modelid=ms.modelid "+
				" inner join productmaster pm on pm.productid=mm.productid "+
				" where ur.userregisterid="+userregisterid+" ";
		try{
			statement=connection.createStatement();
			resultset=statement.executeQuery(sqlString);
			while(resultset.next()){

				
				out.println("<tr>");
				out.println("<td align='left'>"+resultset.getString("productname")+"</td>");
				out.println("<td>"+resultset.getString("modelname")+" "+resultset.getString("modelno")+"</td>");
				out.println("<td align='center'>"+resultset.getString("saledate")+"</td>");
				out.println("<td align='center'>"+resultset.getString("warrantyperiod")+"</td>");
				out.println("<td align='center'>"+resultset.getString("warrantyexpirydate")+"</td>");
				out.println("<td align='center'>"+resultset.getString("complaintsregistered")+"</td>");
				out.println("</tr>");
				
			}

			out.println("</tbody>");
			out.println("</table>");

			
			
		}catch(SQLException se){
			out.println(se);
		}
		
		
		

		out.println("</div>");

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request,response);
	}

}
