

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dbComponents.ConnectDB;

/**
 * Servlet implementation class callDropDown
 */
@WebServlet("/callDropDown")
public class callDropDown extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public callDropDown() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		Connection connection= ConnectDB.newConnection("customersupport");

		//HttpSession session=request.getSession();
		Statement statement;
		ResultSet resultset;
		String sqlString="";
		
		//String username =(String) session.getAttribute("username");
		sqlString	=ConnectDB.getParam(request, "sql");
		
		try{
			statement=connection.createStatement();
			resultset=statement.executeQuery(sqlString);
			while(resultset.next()){

				
				out.println("<option value='"+resultset.getString(1)+"'>"+resultset.getString(2)+"</option>");
				
				
			}

			
			
		}catch(SQLException se){
			out.println(se);
		}
		
		
		

		out.println("</div>");

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request,response);
	}

}
