

import htmlComponents.HTMLComponents;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dbComponents.ConnectDB;

/**
 * Servlet implementation class MachineSale
 */
@WebServlet("/MachineSale")
public class MachineSale extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MachineSale() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		Connection connection= ConnectDB.newConnection("customersupport");

		HttpSession session=request.getSession();
		Statement statement;
		ResultSet resultset;
		String sqlString="",errMsg="";
		PreparedStatement preparedstatement;
		
		String username =(String) session.getAttribute("username");
		int userregisterid =(Integer) session.getAttribute("userregisterid");
		
		
		
		String modelid	=ConnectDB.getParam(request, "modelid");
		String saledate	=ConnectDB.getParam(request, "saledate");
		String button	=ConnectDB.getParam(request, "button");
		
		
		if(button.equals("Add")){
			try {
				
				sqlString=" INSERT INTO machinesale( "+
				" serialno, productid, saledate, modelid, warrantyexpirydate) "+
				" select random_string(15),productid,to_date(?,'dd-mm-yyyy'),modelid,(to_date(?,'dd-mm-yyyy')+warrantyperiod)::date from modelsmaster where modelid=?  ";

						preparedstatement=connection.prepareStatement(sqlString);
						preparedstatement.setString(1,saledate);
						preparedstatement.setString(2,saledate);
						preparedstatement.setInt(3, Integer.parseInt(modelid));
						preparedstatement.executeUpdate();
			}catch (SQLException e) {
				// TODO Auto-generated catch block
				out.println(e);
			}
		}
		
		
		out.println("<div id='box'>");
		out.println("<h3>Product Sale</h3>");
		out.println("<table width='100%'>");
		out.println("<thead>");
		out.println("<tr>");
		out.println("<th >Product</th>");
		out.println("<th >Model</th>");
		out.println("<th >Sale Date</th>");
		out.println("<th >Warranty Expiry Date</th>");
		out.println("<th >Serial Key</th>");
		out.println("<th >Reg. User</th>");
		out.println("</tr>");
		
		out.println("</thead>");
		out.println("<tbody>");
		sqlString="select pm.productname,mm.modelno,to_char(ms.saledate,'dd-mm-yyyy') as saledate,serialno,to_char(ms.warrantyexpirydate,'dd-mm-yyyy') as warrantyexpirydate,coalesce(ur.firstname,'N.A') as reguser from machinesale ms "+
		" inner join productmaster pm on pm.productid=ms.productid "+
		" inner join modelsmaster mm on mm.modelid=ms.modelid "+
		" left join userregister ur on ur.userregisterid=ms.registrationid " ;
		try{
			statement=connection.createStatement();
			resultset=statement.executeQuery(sqlString);
			while(resultset.next()){
				out.println("<tr>");
				out.println("<td align='left'>"+resultset.getString("productname")+"</td>");
				out.println("<td align='left'>"+resultset.getString("modelno")+"</td>");
				out.println("<td align='center'>"+resultset.getString("saledate")+"</td>");
				out.println("<td align='center' >"+resultset.getString("warrantyexpirydate")+"</td>");
				out.println("<td align='center' ><font size='+1' weight='80'>"+resultset.getString("serialno")+"</font></td>");
				out.println("<td align='left' >"+resultset.getString("reguser")+"</td>");
				out.println("</tr>");
			}
		}catch(SQLException se){
			out.println(se);
		}
		
		
		out.println("</tbody>");
		out.println("</table>");

		out.println("</div>");
		
		
		out.println("<div id='box'>");
		out.println("<h3>Add Product Sale</h3>");
		out.println("<form id='form' method='post'>");
		out.println("<fieldset id='model'>");
		out.println("<legend >Product Information :</legend>");
		out.println("<label for='userid'>Model :</label>"); 
		sqlString="select mm.modelname,modelid from modelsmaster mm order by modelname ";
		out.println(HTMLComponents.GenerateDropdown(sqlString,"modelid", "modelname", "modelid", modelid));
		out.println("<br />");                 
		out.println("<label for='firstname'>Sale Date :</label>");
		out.println("<input name='saledate' id='saledate' type='text' tabindex='2' /> (dd-mm-yyyy)");
		out.println("<br />");
		out.println("</fieldset >");
		
		out.println("<div align='center'>");
		out.println("	<input id='button1' name='button' type='button' value='Add' onclick='postForm(\"MachineSale?button=Add\",\"form\")'/>");
		out.println("	<input id='button1' name='button' type='reset' value='Reset' />"); 
		out.println("</div>");
		
		out.println("</form>");
		out.println("</div>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request,response);
	}

}
