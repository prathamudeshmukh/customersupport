package dbComponents;
import java.io.PrintWriter;
import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class ConnectDB {
	public static void main(String[] argv) {
		 
	
	}
	
	public static Connection newConnection(String dbname){
		Connection connection = null;

		try {
 
			Class.forName("org.postgresql.Driver");
 
		} catch (ClassNotFoundException e) {
 
			System.out.println("Where is your PostgreSQL JDBC Driver? Include in your library path!");
			e.printStackTrace();
			return null;
 
		} 
		System.out.println("PostgreSQL JDBC Driver Registered!");
		 
		try {
 
			connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/"+dbname, "postgres","napster");
 
		} catch (SQLException e) {
 
			System.out.println("Connection Failed! Check output console");
			e.printStackTrace();
			return null;
 
		}
 
		
		return connection;
	}
	
	public static String getParam(HttpServletRequest request,String param){
		return (request.getParameter(param)!=null)?request.getParameter(param):"";
	}
	
	public static String getActionString(String servletname){
		return "http://localhost:8080/customersupport/"+servletname;
	}
	
	public static String getResult(String sqlString){
		String returnVal="";
		Statement statement;
		ResultSet resultset;
		Connection connection= ConnectDB.newConnection("customersupport");
		
				try{
					statement=connection.createStatement();
					resultset=statement.executeQuery(sqlString);
					if(resultset.next()){
						returnVal=resultset.getString(1);
					}
				}catch(SQLException se){
					System.out.print("getResult:"+se);
				}
				return returnVal;
	}
	
	public static void getHTMLFooter(PrintWriter out,HttpSession session){
		out.println("         <div id='footer'> ");
		if(session.getAttribute("username")!=null){
			out.println("         <div id='credits'>");
			out.println("         <a href='"+getActionString("loginPage")+"'>Sign Out</a>");
			out.println("         </div>");
		}
		out.println("         <div id='styleswitcher'> ");
		out.println("             <ul> ");
		out.println("                 <li><a href=\"javascript: document.cookie='theme='; window.location.reload();\" title='Default' id='defswitch'>d</a></li> ");
		out.println("                 <li><a href=\"javascript: document.cookie='theme=1'; window.location.reload();\" title='Blue' id='blueswitch'>b</a></li> ");
		out.println("                 <li><a href=\"javascript: document.cookie='theme=2'; window.location.reload();\" title='Green' id='greenswitch'>g</a></li> ");
		out.println("                 <li><a href=\"javascript: document.cookie='theme=3'; window.location.reload();\" title='Brown' id='brownswitch'>b</a></li> ");
		out.println("                 <li><a href=\"javascript: document.cookie='theme=4'; window.location.reload();\" title='Mix' id='mixswitch'>m</a></li> ");
		out.println("             </ul> ");
		out.println("         </div><br /> ");
		out.println("  ");
		out.println("         </div> ");
	}
	
}
